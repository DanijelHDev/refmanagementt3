package hr.tvz.naprednajava.refmanagementt3.controllers.nastavnik;

import hr.tvz.naprednajava.refmanagementt3.entities.Teacher;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class NastavnikAddEditController implements Initializable{

    @FXML
    public TextField ime, prezime, oib, mjesto;
    @FXML
    public TextField dateOfBirth;
    @FXML public Button buttSave, buttCancel;

    private Teacher nastavnik;
    private Stage dialogStage;
    private boolean okClicked = false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public NastavnikAddEditController(){}

    public void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }

    public void setNastavnik(Teacher nastavnik){
        this.nastavnik = nastavnik;

        ime.setText(nastavnik.getName());
        prezime.setText(nastavnik.getSurname());
        oib.setText(nastavnik.getJmbg());
        mjesto.setText(nastavnik.getCity());
        dateOfBirth.setText(nastavnik.getBirthday().toString());
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void Save() {
        nastavnik.setName(ime.getText());
        nastavnik.setSurname(prezime.getText());
        nastavnik.setCity(mjesto.getText());
        nastavnik.setJmbg(oib.getText());

        okClicked = true;
        dialogStage.close();
    }

    @FXML
    private void Cancel(){
        dialogStage.close();
    }
}