package hr.tvz.naprednajava.refmanagementt3.controllers.referada;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.entities.Notice;
import hr.tvz.naprednajava.refmanagementt3.entities.User;
import hr.tvz.naprednajava.refmanagementt3.listeners.OnNoticeArchivedListener;
import hr.tvz.naprednajava.refmanagementt3.listeners.OnNoticeDeletedListener;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import hr.tvz.naprednajava.refmanagementt3.utils.NoticesUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Created by etombla on 4.7.2015..
 */
public class ReferadaObavijestiController implements Initializable, OnNoticeDeletedListener {
    private final static Logger LOGGER = Logger.getLogger(ReferadaObavijestiController.class.getSimpleName());

    @FXML
    public Label nameLabel;
    @FXML
    public VBox noticesContainerVBox;
    @FXML
    public VBox archivedNoticesContainerVBox;

    @FXML
    public ComboBox<String> collegiumComboBox;
    @FXML
    public TextField noticeTitleTextField;
    @FXML
    public TextArea noticeBodyTextArea;
    @FXML
    public DatePicker expirationDatePicker;

    private List<Notice> noticeList;
    private List<Collegium> collegiumsList;

    private Stage window;

    @FXML
    public Button bttnObavijesti, bttnKolegiji, bttnStudenti, bttnNastavnici, bttnPodaci, bttnOdjava;
    public ReferadaObavijestiController(){}

    @FXML
    public void redirect(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnObavijesti) {
            window = (Stage) bttnObavijesti.getScene().getWindow();
            ManageScreens.switchToReferadaObavijestiScreen(window);
        } else if (actionEvent.getSource() == bttnKolegiji) {
            window = (Stage) bttnKolegiji.getScene().getWindow();
            ManageScreens.switchToReferadaKolegijScreen(window);
        } else if (actionEvent.getSource() == bttnStudenti) {
            window = (Stage) bttnStudenti.getScene().getWindow();
            ManageScreens.switchToReferadaStudentiScreen(window);
        } else if (actionEvent.getSource() == bttnNastavnici) {
            window = (Stage) bttnNastavnici.getScene().getWindow();
            ManageScreens.switchToReferadaNastavniciScreen(window);
        } else if (actionEvent.getSource() == bttnPodaci) {
            window = (Stage) bttnPodaci.getScene().getWindow();
            ManageScreens.switchToReferadaPodaciScreen(window);
        } else if (actionEvent.getSource() == bttnOdjava) {
            window = (Stage) bttnOdjava.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LOGGER.info("ReferadaObavijestiController initialization started");
        initStudentName();
        retrieveNoticeList();
        retrieveCollegiumsList();
        initNoticeList();
        NoticesUtils.initCollegiumComboBox(collegiumComboBox, collegiumsList);
        LOGGER.info("ReferadaObavijestiController initialization finished");
    }

    public void initStudentName() {
        User currentUser = CurrentUser.GetCurrentUser();

        String fullname = currentUser.getName();
        fullname += " " + currentUser.getSurname();

        LOGGER.info("User " + fullname + " entered notices screen");
        nameLabel.setText(fullname);
    }

    public void retrieveNoticeList() {
        noticeList = RefRepository.getAllNotices();
        LOGGER.info(noticeList.size() + " notices retrieved");
    }

    public void retrieveCollegiumsList() {
        collegiumsList = RefRepository.getAllCollegiums();
        LOGGER.info(collegiumsList.size() + " collegiums retrieved");
    }

    public void initNoticeList() {
        noticesContainerVBox.getChildren().clear();
        archivedNoticesContainerVBox.getChildren().clear();

        List<VBox> validNoticesWrapperList = new ArrayList<>();
        List<VBox> archivedNoticesWrapperList = new ArrayList<>();

        for (Notice n : noticeList) {
            VBox noticeWrapper = NoticesUtils.createNoticeAdminVBox(n, this);
            if (n.isArhived()) {
                archivedNoticesWrapperList.add(noticeWrapper);
            } else {
                validNoticesWrapperList.add(noticeWrapper);
            }

        }
        noticesContainerVBox.getChildren().addAll(validNoticesWrapperList);
        archivedNoticesContainerVBox.getChildren().addAll(archivedNoticesWrapperList);
        LOGGER.info("Notice container populated");
    }

    @Override
    public void onNoticeDeleted(Notice notice) {
        noticeList.remove(notice);
        RefRepository.deleteNotice(notice);
        LOGGER.info("Notice" + notice.getId() + " deleted");

        try {
            ManageScreens.switchToReferadaObavijestiScreen((Stage) nameLabel.getScene().getWindow());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void submitNotice() {
        User user = CurrentUser.GetCurrentUser();
        Collegium collegium = NoticesUtils.getCollegiumFromComboBox(collegiumComboBox, collegiumsList);
        String title = noticeTitleTextField.getText();
        String body = noticeBodyTextArea.getText();
        Date dateCreated = new Date();

        LocalDate localDate = expirationDatePicker.getValue();
        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
        Date expirationDate = Date.from(instant);

        Notice notice = new Notice(user, collegium, title, body, dateCreated, expirationDate);
        RefRepository.insertNotice(notice);
        LOGGER.info("Notice" + notice.getId() + " created");

        try {
            ManageScreens.switchToReferadaObavijestiScreen((Stage) nameLabel.getScene().getWindow());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Notice> getNoticeList() {
        return noticeList;
    }

    public List<Collegium> getCollegiumsList() {
        return collegiumsList;
    }
}
