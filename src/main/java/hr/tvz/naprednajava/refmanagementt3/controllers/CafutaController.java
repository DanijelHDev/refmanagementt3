package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.entities.Cafuta;
import hr.tvz.naprednajava.refmanagementt3.main.Main;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.Date;

public class CafutaController implements Initializable {
    @FXML
    Button buttMojiPodaci;
    @FXML
    private Button buttPovratak;
    @FXML
    private MenuItem buttOdjava;
    @FXML
    private MenuItem buttTheme;
    @FXML
    private Button buttCreateAdmin;
    @FXML
    private Button createAdmin;
    @FXML
    private Button cancelAdmin;
    @FXML
    private javafx.scene.control.TextField firstNameTextField;
    @FXML
    private javafx.scene.control.TextField lastNameTextField;
    @FXML
    private javafx.scene.control.TextField usernameTextField;
    @FXML
    private javafx.scene.control.TextField passwordTextField;
    @FXML
    private javafx.scene.control.TextField retypePasswordTextField;
    @FXML
    private javafx.scene.control.TextField jmbgTextField;
    @FXML
    private javafx.scene.control.TextField postalCodeTextField;
    @FXML
    private javafx.scene.control.TextField cityTextField;
    @FXML
    private javafx.scene.control.TextField addressTextField;
    @FXML
    private javafx.scene.control.DatePicker birthdayPicker;

    private Stage window;

    @FXML
    public void RedirectMe(ActionEvent event) throws IOException {
        ManageScreens mng = new ManageScreens();
        if (event.getSource() == buttPovratak) {
            window = (Stage) buttPovratak.getScene().getWindow();
            mng.switchToWelcomeScreen(window);
        } else if (event.getSource() == buttOdjava) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        } else if (event.getSource() == buttCreateAdmin) {
            window = (Stage) buttCreateAdmin.getScene().getWindow();
            ManageScreens.switchToCreateAdminScreen(window);
        } else if (event.getSource() == cancelAdmin) {
            window = (Stage) cancelAdmin.getScene().getWindow();
            ManageScreens.switchToAdministratorScreen(window);
        }
    }

    @Override
    public void initialize(java.net.URL location, java.util.ResourceBundle resources) {
    }

    @FXML
    private void createAdmin(ActionEvent event) throws IOException {
        EntityManager entityManager = Main.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(
                new Cafuta(firstNameTextField.getText(), lastNameTextField.getText(),
                        usernameTextField.getText(),
                        passwordTextField.getText(), jmbgTextField.getText(),
                        new Date(birthdayPicker.getValue().toEpochDay()),
                        addressTextField.getText(), cityTextField.getText(),
                        postalCodeTextField.getText(),"true"));

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @FXML
    public void SwitchTheme(ActionEvent event) throws IOException {
        window = (Stage) buttMojiPodaci.getScene().getWindow();
        ManageScreens.SwitchTheme();
        ManageScreens.switchToAdministratorScreen(window);
    }
}
