package hr.tvz.naprednajava.refmanagementt3.controllers.referada;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.AdministrationEmployee;
import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.entities.Student;
import hr.tvz.naprednajava.refmanagementt3.entities.User;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class ReferadaStudentiController implements Initializable {
    @FXML
    Label fullNameLabel;
    @FXML
    private Button bttnMyNotices;
    @FXML
    private Button bttnSubjects;
    @FXML
    private Button bttnStudents;
    @FXML
    private Button bttnProfesors;
    @FXML
    private Button bttnMyInformation;
    @FXML
    private Button bttnLogout;
    @FXML
    private Button bttnCreateNewStudent;
    @FXML
    private TableView<Student> studentsTableView;
    @FXML
    private TableColumn<Student, String> studentsSurnameColumn, studentsNameColumn;
    @FXML
    private DatePicker birthdayPicker;
    @FXML
    private TextField cityTextField;
    @FXML
    private Button bttnEditStudent;
    @FXML
    private Button bttnUpdateStudent;
    @FXML
    private Button bttnIsActive;
    @FXML
    private TextField firstNameTextField;
    @FXML
    private TextField lastNameTextField;
    @FXML
    private TextField addressTextField;
    @FXML
    private TextField jmbagTextField;
    @FXML
    private TextField jmbgTextField;
    @FXML
    private TextField postalCodeTextField;
    @FXML
    private TableView<Collegium> studentsSubjectsTableView;
    @FXML
    private TableColumn<Collegium, String> subjectNameTableColumn;
    @FXML
    private ChoiceBox<String> subjectChoiceBox;
    @FXML
    private Button bttnAddStudentToSubject;
    @FXML
    private Button bttnRemoveStudentFromSubject;

    @FXML
    private TableView<Collegium> tableCollegiums;

    @FXML
    private TableColumn<Collegium, String> columnName;
    @FXML
    private TableColumn<Collegium, String> columnEcts;
    @FXML
    TableColumn<Collegium, String> columnSemester;

    public ReferadaStudentiController() {
    }

    private Stage window;
    private User currentUser = new AdministrationEmployee();
    private Student selectedUser;

    @FXML
    public Button bttnObavijesti, bttnKolegiji, bttnStudenti, bttnNastavnici, bttnPodaci, bttnOdjava;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentUser = (AdministrationEmployee) CurrentUser.GetCurrentUser();
        fullNameLabel.setText(currentUser.getName() + " " + currentUser.getSurname());

        try {
            getStudentsAndFillTable();
        } catch (IOException e) {
            e.printStackTrace();
        }
        disableInputFields(true);
        showStudentDetails(null);
        columnName.setCellValueFactory(new PropertyValueFactory<Collegium, String>("name"));
        columnSemester.setCellValueFactory(new PropertyValueFactory<Collegium, String>("semester"));
        columnEcts.setCellValueFactory(new PropertyValueFactory<Collegium, String>("ects"));

        studentsTableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showStudentDetails(newValue));

    }

    @FXML
    public void redirect(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnObavijesti) {
            window = (Stage) bttnObavijesti.getScene().getWindow();
            ManageScreens.switchToReferadaObavijestiScreen(window);
        } else if (actionEvent.getSource() == bttnKolegiji) {
            window = (Stage) bttnKolegiji.getScene().getWindow();
            ManageScreens.switchToReferadaKolegijScreen(window);
        } else if (actionEvent.getSource() == bttnStudenti) {
            window = (Stage) bttnStudenti.getScene().getWindow();
            ManageScreens.switchToReferadaStudentiScreen(window);
        } else if (actionEvent.getSource() == bttnNastavnici) {
            window = (Stage) bttnNastavnici.getScene().getWindow();
            ManageScreens.switchToReferadaNastavniciScreen(window);
        } else if (actionEvent.getSource() == bttnPodaci) {
            window = (Stage) bttnPodaci.getScene().getWindow();
            ManageScreens.switchToReferadaPodaciScreen(window);
        } else if (actionEvent.getSource() == bttnOdjava) {
            window = (Stage) bttnOdjava.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }

    public void createNewStudent(ActionEvent actionEvent) throws IOException {
//        disableInputFields(false);
//        showStudentDetails(null);
        Student tempStudent = ManageScreens.switchToReferadaStudentAdd(window);
        if(tempStudent != null){
            RefRepository.addUser(tempStudent);
            getStudentsAndFillTable();
        }
    }

    public void editStudent(ActionEvent actionEvent) {
        disableInputFields(false);
    }

    public void updateStudent(ActionEvent actionEvent) throws IOException {
        disableInputFields(true);
        Student user = studentsTableView.getSelectionModel().getSelectedItem();
        user.setName(firstNameTextField.getText());
        user.setSurname(lastNameTextField.getText());
        user.setJmbag(jmbagTextField.getText());
        user.setJmbg(jmbgTextField.getText());
        LocalDate localDate = birthdayPicker.getValue();
        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
        Date birthday = Date.from(instant);
        user.setBirthday(birthday);
        user.setAddress(addressTextField.getText());
        user.setCity(cityTextField.getText());
        user.setPostalCode(postalCodeTextField.getText());

        RefRepository.updateUser(user);


        window = (Stage) bttnUpdateStudent.getScene().getWindow();
        ManageScreens.switchToReferadaStudentiScreen(window);
    }

    public void deleteStudent(ActionEvent actionEvent) throws IOException {
        disableInputFields(true);
        showStudentDetails(null);
    }

    @FXML
    public void addStudentToSubject(ActionEvent actionEvent) {
        Student user = studentsTableView.getSelectionModel().getSelectedItem();
        String collegiumName = subjectChoiceBox.getSelectionModel().selectedItemProperty().getValue();

        Collegium collegium = null;
        for (Collegium c : RefRepository.getAllCollegiums()) {
            if (c.getName().equals(collegiumName)) {
                collegium = c;
            }
        }
        user.getCollegiums().add(collegium);
        RefRepository.updateUser(user);
        ObservableList<Collegium> allCollegiums = FXCollections.observableArrayList(user.getCollegiums());
        tableCollegiums.setItems(allCollegiums);

    }


    @FXML
    public void getStudentsAndFillTable() throws IOException {

        ObservableList<Student> listaZaPrikaz = FXCollections.observableArrayList(RefRepository.getAllStudents());

        studentsNameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("fullName"));
        studentsTableView.setItems(listaZaPrikaz);
    }

    private void disableInputFields(boolean disable) {
        birthdayPicker.setDisable(disable);
        cityTextField.setDisable(disable);
        firstNameTextField.setDisable(disable);
        lastNameTextField.setDisable(disable);
        addressTextField.setDisable(disable);
        jmbgTextField.setDisable(disable);
        jmbagTextField.setDisable(disable);
        postalCodeTextField.setDisable(disable);
    }

    private void showStudentDetails(Student student) {
        if (student != null) {
            birthdayPicker.setValue(
                    Instant.ofEpochMilli(student.getBirthday().getTime()).
                            atZone(ZoneId.systemDefault()).toLocalDate()
            );
            cityTextField.setText(student.getCity());
            firstNameTextField.setText(student.getName());
            lastNameTextField.setText(student.getSurname());
            addressTextField.setText(student.getAddress());
            jmbgTextField.setText(student.getJmbg());
            jmbagTextField.setText(student.getJmbag());
            postalCodeTextField.setText(student.getPostalCode());

            selectedUser = student;

        } else {
            birthdayPicker.setValue(
                    Instant.ofEpochMilli(new Date().getTime()).
                            atZone(ZoneId.systemDefault()).toLocalDate()
            );
            cityTextField.setText("");
            firstNameTextField.setText("");
            lastNameTextField.setText("");
            addressTextField.setText("");
            jmbgTextField.setText("");
            jmbagTextField.setText("");
            postalCodeTextField.setText("");

            selectedUser = new Student();
        }

    }

    @FXML
    public void getCollegiumsForSelectedStudent() {
        Student user = studentsTableView.getSelectionModel().getSelectedItem();
        ObservableList<Collegium> collegiumsList = FXCollections.observableArrayList(user.getCollegiums());
        ObservableList<Collegium> allCollegiums = FXCollections.observableArrayList(RefRepository.getAllCollegiums());

        columnName.setCellValueFactory(new PropertyValueFactory<Collegium, String>("name"));
        columnSemester.setCellValueFactory(new PropertyValueFactory<Collegium, String>("semester"));
        columnEcts.setCellValueFactory(new PropertyValueFactory<Collegium, String>("ects"));

        List<String> nameList = allCollegiums.stream().map(Collegium::getName).collect(Collectors.toList());
        ObservableList<String> collegiumNameList = FXCollections.observableArrayList(nameList);


        subjectChoiceBox.setItems(collegiumNameList);
        tableCollegiums.setItems(collegiumsList);


    }

    @FXML
    public void activateDeactivateUser() {


    }

    @FXML
    public void removeStudentFromSubject() {
        Student user = studentsTableView.getSelectionModel().getSelectedItem();
        String collegiumName = subjectChoiceBox.getSelectionModel().selectedItemProperty().getValue();

        Collegium collegium = null;
        for (Collegium c : RefRepository.getAllCollegiums()) {
            if (c.getName().equals(collegiumName)) {
                collegium = c;
            }
        }
        user.getCollegiums().remove(collegium);
        RefRepository.updateUser(user);
        ObservableList<Collegium> allCollegiums = FXCollections.observableArrayList(user.getCollegiums());
        tableCollegiums.setItems(allCollegiums);
    }


}
