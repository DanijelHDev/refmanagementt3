package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.entities.Student;
import hr.tvz.naprednajava.refmanagementt3.entities.User;
import hr.tvz.naprednajava.refmanagementt3.main.Main;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Danijel on 19.5.2015..
 */
public class StudentController {

    @FXML
    private Button buttPovratak;
    @FXML
    Button buttMojiPodaci;
    @FXML
    Button buttObavijesti;
    private Stage window;
    @FXML
    Button buttonDodajStudenta;

    @FXML
    private TableView<Student> studentTableView;
    @FXML
    private TableColumn<Student, Integer> idColumn;
    @FXML
    private TableColumn<Student, String> nameColumn;
    @FXML
    private TableColumn<Student, String> surnameColumn;
    @FXML
    private Label jmbgLabel;
    @FXML
    private Label jmbagLabel;
    @FXML
    private Label usernameLabel;
    @FXML
    private Label smjerLabel;
    @FXML
    private Label addressLabel;
    @FXML
    private Label birthdayLabel;
    @FXML
    private Label cityLabel;
    @FXML
    private Label postalCodeLabel;
    @FXML
    private ObservableList<Student> studentData;


    //Kreiranje studenta

    @FXML
    private TextField nameTextField;
    @FXML
    private TextField surnameTextField;
    @FXML
    private TextField jmbgTextField;
    @FXML
    private TextField jmbagTextField;
    @FXML
    private TextField usernameTextField;
    @FXML
    private TextField addressTextField;
    @FXML
    private TextField cityTextField;
    @FXML
    private TextField postalCodeTextField;
    @FXML
    private DatePicker birthdayDatePicker;
    @FXML
    private PasswordField passwordFied;
    @FXML
    Button saveStudentButton;


    @FXML
    public void RedirectMe(ActionEvent event) throws IOException {
        if (event.getSource() == buttPovratak) {
            window = (Stage) buttPovratak.getScene().getWindow();
            ManageScreens.switchToWelcomeScreen(window);
        } else if (event.getSource() == buttMojiPodaci) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToMojiPodaciScreen(window);
        } else if (event.getSource() == buttObavijesti) {
            window = (Stage) buttObavijesti.getScene().getWindow();
            ManageScreens.switchToObavijestiScreen(window);
        } else if (event.getSource() == buttonDodajStudenta) {
            window = (Stage) buttonDodajStudenta.getScene().getWindow();
            ManageScreens.switchToKreirajStudentaScreen(window);
        }
    }


    @FXML
    public void initialize() {
        if (studentTableView == null) {
            getCourses();
        } else {
            getStudentList();
            idColumn.setCellValueFactory(new PropertyValueFactory<Student, Integer>("id"));
            nameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("name"));
            surnameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("surname"));

            getStudentDetails(null);
            studentTableView.getSelectionModel().selectedItemProperty().addListener(
                    (observable, oldValue, newValue) -> getStudentDetails(newValue));

//                jmbagColumn.setCellValueFactory(new PropertyValueFactory<student, String>("jmbag"));
//                jmbgColumn.setCellValueFactory(new PropertyValueFactory<student, String>("jmbg"));
//                usernameColumn.setCellValueFactory(new PropertyValueFactory<student, String>("username"));
//                addressColumn.setCellValueFactory(new PropertyValueFactory<student, String>("address"));
//                cityColumn.setCellValueFactory(new PropertyValueFactory<student, String>("city"));
//                postalCodeColumn.setCellValueFactory(new PropertyValueFactory<student, String>("postal_code"));
        }


    }


    @FXML
    public void getStudentList() {
        EntityManager entityManager = Main.getEntityManager();
        entityManager.getTransaction().begin();
        studentData = FXCollections.observableArrayList(entityManager.createQuery("from Student", Student.class).getResultList());
        entityManager.getTransaction().commit();
        entityManager.close();
        studentTableView.setItems(studentData);
    }

    @FXML
    public void getCourses() {
        EntityManager entityManager = Main.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.getTransaction().commit();
        entityManager.close();

    }

    private void getStudentDetails(Student student) {
        if (student != null) {
            jmbgLabel.setText(student.getJmbg());
            jmbagLabel.setText(student.getJmbag());
            usernameLabel.setText(student.getUsername());
            birthdayLabel.setText(student.getBirthday().toString());
            addressLabel.setText(student.getAddress());
            cityLabel.setText(student.getCity());
            postalCodeLabel.setText(student.getPostalCode());
        } else {
            jmbgLabel.setText("");
            jmbagLabel.setText("");
            usernameLabel.setText("");
            smjerLabel.setText("");
            birthdayLabel.setText("");
            addressLabel.setText("");
            cityLabel.setText("");
            postalCodeLabel.setText("");
        }
    }


    @FXML
    public void createNewStudent(ActionEvent event) throws IOException {
        EntityManager entityManager = Main.getEntityManager();
        entityManager.getTransaction().begin();
        Date birthday = new Date(birthdayDatePicker.getValue().toEpochDay());
        String name = nameTextField.getText();
        //grdi hack
        User student = new Student(nameTextField.getText(), surnameTextField.getText(), usernameTextField.getText(), passwordFied.getText(), jmbgTextField.getText(), birthday,
                addressTextField.getText(), cityTextField.getText(), postalCodeTextField.getText(), jmbagTextField.getText(),"true");
        entityManager.persist(student);
        entityManager.getTransaction().commit();
        entityManager.close();
        window = (Stage) nameTextField.getScene().getWindow();
        try {
            ManageScreens.switchToStudentScreen(window);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}