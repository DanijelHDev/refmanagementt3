package hr.tvz.naprednajava.refmanagementt3.controllers.student;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Student;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StudentPodaciController implements Initializable {
    @FXML
    private Label fullNameLabel;
    @FXML
    private Button bttnMyNotices;
    @FXML
    private Button bttnMySubjects;
    @FXML
    private Button bttnMyGrades;
    @FXML
    private Button bttnMyInformation;
    @FXML
    private Button bttnLogout;
    @FXML
    private Button bttnChangePassword;
    @FXML
    private TextField oldPasswordTextField;
    @FXML
    private TextField newPasswordTextField;
    @FXML
    private TextField newPasswordReenterTextField;

    private Stage window;
    private Student currentUser = new Student();

    public StudentPodaciController(){}

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentUser = (Student) CurrentUser.GetCurrentUser();
        fullNameLabel.setText(currentUser.getName() + " " + currentUser.getSurname());
        bttnChangePassword.setDisable(false);

    }

    @FXML
    public void redirect(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnMyNotices) {
            window = (Stage) bttnMyNotices.getScene().getWindow();
            ManageScreens.switchToStudentObavijestiScreen(window);

        } else if (actionEvent.getSource() == bttnMySubjects) {
            window = (Stage) bttnMySubjects.getScene().getWindow();
            ManageScreens.switchToStudentKolegijiScreen(window);

        } else if (actionEvent.getSource() == bttnMyGrades) {
            window = (Stage) bttnMyGrades.getScene().getWindow();
            ManageScreens.switchToStudentOcjeneScreen(window);

        } else if (actionEvent.getSource() == bttnMyInformation) {
            window = (Stage) bttnMyInformation.getScene().getWindow();
            ManageScreens.switchToStudentPodaciScreen(window);

        } else if (actionEvent.getSource() == bttnLogout) {
            window = (Stage) bttnLogout.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);

        }
    }

    @FXML
    public void changePassword(ActionEvent actionEvent) throws IOException {
        if (!isAnyFieldEmpty()) {
            if (newPasswordTextField.getText().equals(newPasswordReenterTextField.getText())
                    && oldPasswordTextField.getText().equals(currentUser.getPassword())) {

                currentUser.setPassword(newPasswordTextField.getText());
                RefRepository.updateUser(currentUser);

                window = (Stage) bttnMyNotices.getScene().getWindow();
                ManageScreens.switchToStudentObavijestiScreen(window);
            } else {
                window = (Stage) bttnMyInformation.getScene().getWindow();
                ManageScreens.switchToMojiPodaciScreen(window);
            }
        }
    }

    private Boolean isAnyFieldEmpty() {
        if (oldPasswordTextField.getText().isEmpty() ||
                newPasswordTextField.getText().isEmpty() ||
                newPasswordReenterTextField.getText().isEmpty()) {
            return true;
        }
        return false;
    }
}
