package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Danijel on 19.5.2015..
 */
public class MojiPodaciController {

    @FXML
    private Button buttPovratak;
    @FXML
    Button buttMojiPodaci;
    @FXML
    Button buttObavijesti;
    @FXML
    private MenuItem buttOdjava;
    @FXML
    private MenuItem buttTheme;
    private Stage window;

    @FXML
    public void RedirectMe(ActionEvent event) throws IOException {
        if (event.getSource() == buttPovratak) {
            window = (Stage) buttPovratak.getScene().getWindow();
            ManageScreens.switchToWelcomeScreen(window);
        } else if (event.getSource() == buttObavijesti) {
            window = (Stage) buttObavijesti.getScene().getWindow();
            ManageScreens.switchToObavijestiScreen(window);
        } else if (event.getSource() == buttOdjava) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }

    @FXML
    public void SwitchTheme(ActionEvent event) throws IOException {
        window = (Stage) buttMojiPodaci.getScene().getWindow();
        ManageScreens.SwitchTheme();
        ManageScreens.switchToMojiPodaciScreen(window);
    }

}
