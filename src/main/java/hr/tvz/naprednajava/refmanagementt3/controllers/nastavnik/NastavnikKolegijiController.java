package hr.tvz.naprednajava.refmanagementt3.controllers.nastavnik;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.*;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Ref;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Frane on 3.7.2015..
 */
public class NastavnikKolegijiController implements Initializable {
    @FXML
    public Label labelIme, labelSemestar, labelECTS, labelOpis, labelIzracunOcjene, fullNameLabel;
    @FXML
    public TableView<Collegium> tablicaKolegija;
    @FXML
    public TableView<Student> tablicaStudenata;
    @FXML
    public TableColumn<Collegium, String> kolonaImeKolegija;
    @FXML
    public TableColumn<Student, String> kolonaImeStudenta, kolonaPrezimeStudenta;
    @FXML
    public Button buttMojeObavijesti, buttKolegiji, buttMojiPodaci, buttOdjava, buttOsvjeziIzracun, buttSpremiOcjeni;
    @FXML
    public TextField textFieldBodoviLabosi, textFieldBodoviKolokviji, textFieldBodoviDodatni, dovoljanMin, dovoljanMax,
            dobarMin, dobarMax, vrloDobarMin, vrloDobarMax, izvrsanMin, izvrsanMax;

    int grade = 1;
    private Stage window;
    private Teacher currentUser;
    private Collegium selectedCollegium;
    private Student selectedStudent;
    private List<Collegium> listaKolegija = new List<Collegium>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<Collegium> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(Collegium collegium) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Collegium> c) {
            return false;
        }

        @Override
        public boolean addAll(int index, Collection<? extends Collegium> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public Collegium get(int index) {
            return null;
        }

        @Override
        public Collegium set(int index, Collegium element) {
            return null;
        }

        @Override
        public void add(int index, Collegium element) {

        }

        @Override
        public Collegium remove(int index) {
            return null;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @Override
        public ListIterator<Collegium> listIterator() {
            return null;
        }

        @Override
        public ListIterator<Collegium> listIterator(int index) {
            return null;
        }

        @Override
        public List<Collegium> subList(int fromIndex, int toIndex) {
            return null;
        }
    };

    public NastavnikKolegijiController() {
    }

    @FXML
    public void redirectMe(ActionEvent event) throws IOException {
        if (event.getSource() == buttMojeObavijesti) {
            window = (Stage) buttMojeObavijesti.getScene().getWindow();
            ManageScreens.switchToNastavnikObavijestiScreen(window);
        } else if (event.getSource() == buttKolegiji) {
            window = (Stage) buttKolegiji.getScene().getWindow();
            ManageScreens.switchToNastavnikKolegijiScreen(window);
        } else if (event.getSource() == buttMojiPodaci) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToNastavnikPodaciScreen(window);
        } else if (event.getSource() == buttOdjava) {
            window = (Stage) buttOdjava.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentUser = (Teacher) CurrentUser.GetCurrentUser();
        fullNameLabel.setText(currentUser.getName() + " " + currentUser.getSurname());

        try {
            dohvatiListuKolegija();
        } catch (IOException e) {
            e.printStackTrace();
        }
        prikaziDetaljeKolegija(null);
        tablicaKolegija.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    prikaziDetaljeKolegija(newValue);
                    try {
                        tablicaStudenata.getItems().clear();
                        dohvatiListuStudenata();
                        dohvatiListuPragova();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        tablicaStudenata.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    selectedStudent = newValue;
                    dohvatiBodoveStudenta();
                });
    }

    private void dohvatiListuPragova() {
        List<Tresholds> tresholds = selectedCollegium.getTresholds();

        for (Tresholds t : tresholds) {
            switch(t.getScore()) {
                case 2:
                    dovoljanMin.setText(Integer.toString(t.getMinPoints()));
                    dovoljanMax.setText(Integer.toString(t.getMaxPoints()));
                    break;
                case 3:
                    dobarMin.setText(Integer.toString(t.getMinPoints()));
                    dobarMax.setText(Integer.toString(t.getMaxPoints()));
                    break;
                case 4:
                    vrloDobarMin.setText(Integer.toString(t.getMinPoints()));
                    vrloDobarMax.setText(Integer.toString(t.getMaxPoints()));
                    break;
                case 5:
                    izvrsanMin.setText(Integer.toString(t.getMinPoints()));
                    izvrsanMax.setText(Integer.toString(t.getMaxPoints()));
                    break;
            }
        }
    }

    private void dohvatiBodoveStudenta() {
        List<Point> points = selectedStudent.getStudentPoints().stream()
                .filter(point -> point.getCollegium().getId().equals(selectedCollegium.getId()))
                .collect(Collectors.toList());
        resetBodoviTextFields();

        for (Point p : points) {
            switch (p.getType().name()) {
                case "COLLOQUIUM":
                    textFieldBodoviKolokviji.setText(Integer.toString(p.getPoints()));
                    break;
                case "LABORATORY":
                    textFieldBodoviLabosi.setText(Integer.toString(p.getPoints()));
                    break;
                case "EXTRA_POINTS":
                    textFieldBodoviDodatni.setText(Integer.toString(p.getPoints()));
                    break;
            }
        }

        postaviNuleZaNepostojeceBodove();
    }

    private void postaviNuleZaNepostojeceBodove() {
        if (textFieldBodoviKolokviji.getText().isEmpty())
            textFieldBodoviKolokviji.setText("0");
        if (textFieldBodoviLabosi.getText().isEmpty())
            textFieldBodoviLabosi.setText("0");
        if (textFieldBodoviDodatni.getText().isEmpty())
            textFieldBodoviDodatni.setText("0");

    }

    private void resetBodoviTextFields() {
        textFieldBodoviDodatni.setText("");
        textFieldBodoviLabosi.setText("");
        textFieldBodoviKolokviji.setText("");
    }

    @FXML
    public void dohvatiListuKolegija() throws IOException {
        listaKolegija = RefRepository.getAllCollegiums();

        ObservableList<Collegium> listaZaPrikazKolegija = FXCollections.observableArrayList(listaKolegija);

        kolonaImeKolegija.setCellValueFactory(new PropertyValueFactory<Collegium, String>("name"));
        tablicaKolegija.setItems(listaZaPrikazKolegija);
    }

    @FXML
    public void dohvatiListuStudenata() throws IOException {
        ObservableList<Student> listaZaPrikazStudenata = FXCollections.observableArrayList();

        for (Student s : selectedCollegium.getStudents()) {
            if (!listaZaPrikazStudenata.contains(s))
                listaZaPrikazStudenata.add(s);
        }

        kolonaImeStudenta.setCellValueFactory(new PropertyValueFactory<Student, String>("name"));
        kolonaPrezimeStudenta.setCellValueFactory(new PropertyValueFactory<Student, String>("surname"));
        tablicaStudenata.setItems(listaZaPrikazStudenata);
    }

    @FXML
    private void prikaziDetaljeKolegija(Collegium kolegij) {
        if (kolegij != null) {
            labelIme.setText(kolegij.getName());
            labelSemestar.setText(kolegij.getSemester());
            labelECTS.setText(kolegij.getEcts());
            labelOpis.setText(kolegij.getDescription());
            selectedCollegium = kolegij;
        } else {
            labelIme.setText("");
            labelSemestar.setText("");
            labelECTS.setText("");
            labelOpis.setText("");
        }
    }

    @FXML
    private void osvjeziIzracun() {
        List<Tresholds> thresholds = selectedCollegium.getTresholds();

        int sum = Integer.parseInt(textFieldBodoviDodatni.getText()) +
                Integer.parseInt(textFieldBodoviKolokviji.getText()) +
                Integer.parseInt(textFieldBodoviLabosi.getText());

        if (sum < getLowestThreshold(thresholds)) {
            grade = 1;
        } else if (sum > getHighestThreshold(thresholds)) {
            grade = 5;
        } else {
            thresholds.stream().filter(
                    threshold -> threshold.getMinPoints() <= sum && threshold.getMaxPoints() >= sum
            ).forEach(threshold -> grade = threshold.getScore());
        }
        String stringGrade;

        switch (grade) {
            case 1:
                stringGrade = "Nedovoljan (1)";
                break;
            case 2:
                stringGrade = "Dovoljan (2)";
                break;
            case 3:
                stringGrade = "Dobar (3)";
                break;
            case 4:
                stringGrade = "Vrlo dobar (4)";
                break;
            case 5:
                stringGrade = "Odličan (5)";
                break;
            default:
                stringGrade = "Ocjena nepoznata";
        }

        labelIzracunOcjene.setText(stringGrade);
    }

    @FXML
    private void spremiOcjene() {
        osvjeziIzracun();
        List<Point> pointList = selectedStudent.getStudentPoints().stream()
                .filter(point -> point.getCollegium().getId().equals(selectedCollegium.getId()))
                .collect(Collectors.toList());

        for (Point p : pointList) {
            if(p.getType().equals(Type.COLLOQUIUM)) {
                p.setPoints(Integer.parseInt(textFieldBodoviKolokviji.getText()));
            } else if (p.getType().equals(Type.EXTRA_POINTS)) {
                p.setPoints(Integer.parseInt(textFieldBodoviDodatni.getText()));
            } else if(p.getType().equals(Type.LABORATORY)) {
                p.setPoints(Integer.parseInt(textFieldBodoviLabosi.getText()));
            }
            RefRepository.updatePoints(p);
        }
    }

    private int getLowestThreshold(List<Tresholds> thresholds) {
        return thresholds.stream().min(Comparator.comparing(
                treshold -> treshold.getMinPoints()
        )).get().getMinPoints();
    }

    private int getHighestThreshold(List<Tresholds> thresholds) {
        return thresholds.stream().max(Comparator.comparing(
                treshold -> treshold.getMaxPoints()
        )).get().getMaxPoints();
    }

    @FXML
    private void spremiGranice() {
        List<Tresholds> tresholds = selectedCollegium.getTresholds();

        for (Tresholds t : tresholds) {
            switch(t.getScore()) {
                case 1:
                    t.setMinPoints(0);
                    t.setMaxPoints(Integer.parseInt(dovoljanMin.getText()) - 1);
                    break;
                case 2:
                    t.setMinPoints(Integer.parseInt(dovoljanMin.getText()));
                    t.setMaxPoints(Integer.parseInt(dovoljanMax.getText()));
                    break;
                case 3:
                    t.setMinPoints(Integer.parseInt(dobarMin.getText()));
                    t.setMaxPoints(Integer.parseInt(dobarMax.getText()));
                    break;
                case 4:
                    t.setMinPoints(Integer.parseInt(vrloDobarMin.getText()));
                    t.setMaxPoints(Integer.parseInt(vrloDobarMax.getText()));
                    break;
                case 5:
                    t.setMinPoints(Integer.parseInt(izvrsanMin.getText()));
                    t.setMaxPoints(Integer.parseInt(izvrsanMax.getText()));
                    break;
            }
            RefRepository.updateThresholds(t);
        }
    }
}
