package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.bil.LoginDAOImpl;
import hr.tvz.naprednajava.refmanagementt3.entities.*;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.Main;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;


/**
 * Created by Danijel on 16.5.2015..
 */
public class LoginController {
    @FXML
    TextField txtFieldUsername;
    @FXML
    TextField txtFieldPassword;
    @FXML
    Label lblLoginCheck;
    private LoginDAOImpl loginRepository;
    @FXML
    private Button buttLogin;
    private Stage window;

    @FXML
    public void LoginClick() throws IOException {

        loginRepository = new LoginDAOImpl();
        loginRepository.registerEntityManager(Main.getEntityManager());

        List<User> listausera = loginRepository.getUsers(txtFieldUsername.getText());
        //loginRepository.CloseEntityManager();

        //Provjera ako je lista prazna
        User user = null;
        if (listausera.isEmpty()) {
            lblLoginCheck.setText("Neispravno korisnicko ime i/ili lozinka!");
        } else {
            //Svrstavanje Osobe 'pravom' tipu
            for (User wnbUser : listausera) {
                if (wnbUser instanceof Student) {
                    user = (Student) wnbUser;
                    break;
                } else if (wnbUser instanceof Teacher) {
                    user = (Teacher) wnbUser;
                    break;
                } else if (wnbUser instanceof Cafuta) {
                    user = (Cafuta) wnbUser;
                    break;
                } else if (wnbUser instanceof AdministrationEmployee) {
                    user = (AdministrationEmployee) wnbUser;
                }
            }
            //Provjera za matchanje passworda
            if (user.getPassword().equals(txtFieldPassword.getText())) {
                if (user instanceof Cafuta) {
                    window = (Stage) buttLogin.getScene().getWindow();
                    CurrentUser.SetCurrentUser(user);
                    ManageScreens.switchToAdminDjelatniciScreen(window);
                } else if (user instanceof Teacher) {
                    window = (Stage) buttLogin.getScene().getWindow();
                    CurrentUser.SetCurrentUser(user);
                    ManageScreens.switchToNastavnikObavijestiScreen(window);
                } else if (user instanceof AdministrationEmployee) {
                    window = (Stage) buttLogin.getScene().getWindow();
                    CurrentUser.SetCurrentUser(user);
                    ManageScreens.switchToReferadaObavijestiScreen(window);
                } else {
                    window = (Stage) buttLogin.getScene().getWindow();
                    CurrentUser.SetCurrentUser(user);
                    ManageScreens.switchToStudentObavijestiScreen(window);
                }
            } else {
                lblLoginCheck.setText("Neispravno korisnicko ime i/ili lozinka!");
            }
        }
    }
}
