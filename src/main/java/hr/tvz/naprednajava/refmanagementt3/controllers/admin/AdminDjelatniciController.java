package hr.tvz.naprednajava.refmanagementt3.controllers.admin;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.AdministrationEmployee;
import hr.tvz.naprednajava.refmanagementt3.entities.User;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class AdminDjelatniciController implements Initializable {

    @FXML
    private Button bttnDjelatniciReferade, bttnMojiPodaci, bttnLogout, bttnCreate, bttnEdit, bttnSave, bttnDeactivate;
    @FXML
    private Label fullNameLabel;
    @FXML
    private TableView<AdministrationEmployee> tableDjelatnici;
    @FXML
    private TableColumn<AdministrationEmployee, String> firstNameColumn, lastNameColumn;
    @FXML
    private DatePicker dateOfBirthPicker;
    @FXML
    private TextField cityTextField, firstNameTextField, lastNameTextField, addressTextField, oibTextField;

    private Stage window;
    private AdministrationEmployee selectedDjelatnik;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            dohvatiListuDjelatnika();
        } catch (IOException e) {
            e.printStackTrace();
        }

        disableInputFields(true);
        prikaziDetaljeDjelatnika(null);
        User currentUser = CurrentUser.GetCurrentUser();

        fullNameLabel.setText(currentUser.getName() + " " + currentUser.getSurname());

        tableDjelatnici.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    selectedDjelatnik = newValue;
                    prikaziDetaljeDjelatnika(newValue);
                });
    }

    @FXML
    private void prikaziDetaljeDjelatnika(AdministrationEmployee administrationEmployee) {
        if (administrationEmployee != null) {
            disableInputFields(false);
            addressTextField.setText(administrationEmployee.getAddress());
            cityTextField.setText(administrationEmployee.getCity());
            firstNameTextField.setText(administrationEmployee.getName());
            lastNameTextField.setText(administrationEmployee.getSurname());
            oibTextField.setText(administrationEmployee.getOib());
            dateOfBirthPicker.setValue(LocalDate.from(Instant.from(administrationEmployee.getBirthday().toInstant())));
        } else {
            disableInputFields(true);
            addressTextField.setText("");
            cityTextField.setText("");
            firstNameTextField.setText("");
            lastNameTextField.setText("");
            oibTextField.setText("");
            dateOfBirthPicker.setValue(LocalDate.now());
        }
    }

    @FXML
    private void disableInputFields(boolean disable) {
        addressTextField.setDisable(disable);
        cityTextField.setDisable(disable);
        firstNameTextField.setDisable(disable);
        lastNameTextField.setDisable(disable);
        oibTextField.setDisable(disable);
        dateOfBirthPicker.setDisable(disable);
    }

    @FXML
    private void dohvatiListuDjelatnika() throws IOException {
        ObservableList<AdministrationEmployee> listaZaPrikaz = FXCollections.observableArrayList(RefRepository.getAllEmployees());

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, String>("name"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, String>("surname"));
        tableDjelatnici.setItems(listaZaPrikaz);
    }

    @FXML
    public void redirect(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnDjelatniciReferade) {
            window = (Stage) bttnDjelatniciReferade.getScene().getWindow();
            ManageScreens.switchToAdminDjelatniciScreen(window);

        } else if (actionEvent.getSource() == bttnMojiPodaci) {
            window = (Stage) bttnMojiPodaci.getScene().getWindow();
            ManageScreens.switchToAdminPodaciScreen(window);

        } else if (actionEvent.getSource() == bttnLogout) {
            window = (Stage) bttnLogout.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);

        }
    }

    @FXML
    public void createDjelatnik(ActionEvent actionEvent) throws IOException {
        disableInputFields(false);
        tableDjelatnici.setDisable(true);
        selectedDjelatnik = null;
    }

    @FXML
    public void editDjelatnik(ActionEvent actionEvent) {
        disableInputFields(false);
        tableDjelatnici.setDisable(true);
        prikaziDetaljeDjelatnika(selectedDjelatnik);
    }

    @FXML
    public void deactivateDjelatnik(ActionEvent actionEvent) throws IOException {
        selectedDjelatnik.setIsActive("false");

        RefRepository.createOrUpdateDjelatnik(selectedDjelatnik);

        window = (Stage) bttnDjelatniciReferade.getScene().getWindow();
        ManageScreens.switchToAdminDjelatniciScreen(window);
    }

    @FXML
    public void saveDjelatnik(ActionEvent actionEvent) throws IOException {
        disableInputFields(true);

        LocalDate localDate = dateOfBirthPicker.getValue();
        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
        Date dateOfBirth = Date.from(instant);

        if (selectedDjelatnik != null) {
            selectedDjelatnik.setJmbg("1111111111");
            selectedDjelatnik.setBirthday(dateOfBirth);
            selectedDjelatnik.setSurname(lastNameTextField.getText());
            selectedDjelatnik.setName(firstNameTextField.getText());
            selectedDjelatnik.setOib(oibTextField.getText());
            selectedDjelatnik.setAddress(addressTextField.getText());
            selectedDjelatnik.setCity(cityTextField.getText());
            selectedDjelatnik.setPostalCode("10000");
        } else {
            selectedDjelatnik = new AdministrationEmployee(
                    firstNameTextField.getText(),
                    lastNameTextField.getText(),
                    firstNameTextField.getText().substring(0, 1) + lastNameTextField.getText(),
                    "pass",
                    "1111111111",
                    dateOfBirth,
                    addressTextField.getText(),
                    cityTextField.getText(),
                    "10000",
                    oibTextField.getText(),
                    "true");
        }

        RefRepository.createOrUpdateDjelatnik(selectedDjelatnik);
        tableDjelatnici.setDisable(false);

        window = (Stage) bttnDjelatniciReferade.getScene().getWindow();
        ManageScreens.switchToAdminDjelatniciScreen(window);
    }

}
