package hr.tvz.naprednajava.refmanagementt3.controllers.student;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Created by Frane on 3.7.2015..
 */
public class StudentKolegijiController implements Initializable {
    @FXML
    public Label labelName, labelSemestar, labelECTS, labelDescription,nameLabel;
    @FXML
    public TableView<Collegium> tableCollegiums;
    @FXML
    public TableColumn<Collegium, String> collumnCollegiumName;
    @FXML
    public Button bttnMyNotices, bttnMyCollegiums, bttnMyGrades, bttnMyInformation, bttnLogout;

    private Stage window;

    public StudentKolegijiController(){}

    @FXML
    public void RedirectMe(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnMyNotices) {
            window = (Stage) bttnMyNotices.getScene().getWindow();
            ManageScreens.switchToStudentObavijestiScreen(window);
        } else if (actionEvent.getSource() == bttnMyCollegiums) {
            window = (Stage) bttnMyCollegiums.getScene().getWindow();
            ManageScreens.switchToStudentKolegijiScreen(window);
        } else if (actionEvent.getSource() == bttnMyGrades) {
            window = (Stage) bttnMyGrades.getScene().getWindow();
            ManageScreens.switchToStudentOcjeneScreen(window);
        } else if (actionEvent.getSource() == bttnMyInformation) {
            window = (Stage) bttnMyInformation.getScene().getWindow();
            ManageScreens.switchToStudentPodaciScreen(window);
        } else if (actionEvent.getSource() == bttnLogout) {
            window = (Stage) bttnLogout.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            DohvatiListuCollegiuma();

        } catch (IOException e) {
            e.printStackTrace();
        }
        prikaziDetaljeCollegiuma(null);
        tableCollegiums.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> prikaziDetaljeCollegiuma(newValue));
    }

    @FXML
    public void DohvatiListuCollegiuma() throws IOException {

        List<Collegium> listaCollegiuma = RefRepository.getAllCollegiums();


        ObservableList<Collegium> listaZaPrikaz = FXCollections.observableArrayList(listaCollegiuma);
        nameLabel.setText(CurrentUser.GetCurrentUser().getFullName());
        collumnCollegiumName.setCellValueFactory(new PropertyValueFactory<Collegium, String>("name"));
        tableCollegiums.setItems(listaZaPrikaz);

    }

    private void prikaziDetaljeCollegiuma(Collegium Collegium) {
        if (Collegium != null) {
            labelName.setText(Collegium.getName());
            labelSemestar.setText(Collegium.getSemester());
            labelECTS.setText(Collegium.getEcts());
            labelDescription.setText(Collegium.getDescription());
        } else {
            labelName.setText("");
            labelSemestar.setText("");
            labelECTS.setText("");
            labelDescription.setText("");
        }
    }
}
