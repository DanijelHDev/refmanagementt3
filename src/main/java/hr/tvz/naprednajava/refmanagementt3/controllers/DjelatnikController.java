package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.entities.AdministrationEmployee;
import hr.tvz.naprednajava.refmanagementt3.main.Main;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.Date;

public class DjelatnikController {

    @FXML
    private Button buttPovratak;
    @FXML
    Button buttMojiPodaci;
    @FXML
    Button buttLogout;
    @FXML
    Button buttObavijesti;
    @FXML
    private Button buttCreateDjelatnik;
    @FXML
    private Button createDjelatnik;
    @FXML
    private Button cancelDjelatnik;
    @FXML
    private javafx.scene.control.TextField firstNameTextField;
    @FXML
    private javafx.scene.control.TextField lastNameTextField;
    @FXML
    private javafx.scene.control.TextField usernameTextField;
    @FXML
    private javafx.scene.control.TextField passwordTextField;
    @FXML
    private javafx.scene.control.TextField retypePasswordTextField;
    @FXML
    private javafx.scene.control.TextField jmbgTextField;
    @FXML
    private javafx.scene.control.TextField oibTextField;
    @FXML
    private javafx.scene.control.TextField postalCodeTextField;
    @FXML
    private javafx.scene.control.TextField cityTextField;
    @FXML
    private javafx.scene.control.TextField addressTextField;
    @FXML
    private javafx.scene.control.DatePicker birthdayPicker;

    @FXML
    private MenuItem buttOdjava;

    @FXML
    private MenuItem buttTheme;

    private Stage window;

    @FXML
    private TableView<AdministrationEmployee> djelatnikTableView;
    @FXML
    private TableColumn<AdministrationEmployee, Integer> idColumn;
    @FXML
    private TableColumn<AdministrationEmployee, String> nameColumn;
    @FXML
    private TableColumn<AdministrationEmployee, String> surnameColumn;
    @FXML
    private TableColumn<AdministrationEmployee, String> usernameColumn;
    @FXML
    private TableColumn<AdministrationEmployee, String> oibColumn;
    @FXML
    private TableColumn<AdministrationEmployee, String> jmbgColumn;
    @FXML
    private TableColumn<AdministrationEmployee, String> addressColumn;
    @FXML
    private TableColumn<AdministrationEmployee, String> cityColumn;
    @FXML
    private TableColumn<AdministrationEmployee, String> postalCodeColumn;
    @FXML
    private ObservableList<AdministrationEmployee> administrationEmployeeData;

    @FXML
    public void RedirectMe(ActionEvent event) throws IOException {
        if (event.getSource() == buttPovratak) {
            window = (Stage) buttPovratak.getScene().getWindow();
            ManageScreens.switchToWelcomeScreen(window);
        } else if (event.getSource() == buttMojiPodaci) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToMojiPodaciScreen(window);
        } else if (event.getSource() == buttObavijesti) {
            window = (Stage) buttObavijesti.getScene().getWindow();
            ManageScreens.switchToObavijestiScreen(window);
        } else if (event.getSource() == buttOdjava) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        } else if (event.getSource() == buttCreateDjelatnik) {
            window = (Stage) buttCreateDjelatnik.getScene().getWindow();
            ManageScreens.switchToCreateDjelatnikScreen(window);
        } else if (event.getSource() == cancelDjelatnik) {
            window = (Stage) cancelDjelatnik.getScene().getWindow();
            ManageScreens.switchToCreateDjelatnikScreen(window);
        }
    }

    @FXML
    public void SwitchTheme(ActionEvent event) throws IOException {
        window = (Stage) buttMojiPodaci.getScene().getWindow();
        ManageScreens.SwitchTheme();
        ManageScreens.switchToDjelatnikReferade(window);
    }

    @FXML
    public void getDjelatnikList() {

        EntityManager entityManager = Main.getEntityManager();
        entityManager.getTransaction().begin();
        administrationEmployeeData = FXCollections.observableArrayList(entityManager.createQuery("from Djelatnik", AdministrationEmployee.class).getResultList());
        entityManager.getTransaction().commit();
        entityManager.close();
        djelatnikTableView.setItems(administrationEmployeeData);
    }

    @FXML
    public void initialize() {
        getDjelatnikList();
        idColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, Integer>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, String>("name"));
        surnameColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, String>("surname"));
        usernameColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, String>("username"));
        jmbgColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, String>("jmbg"));
        oibColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, String>("oib"));
        addressColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, String>("address"));
        cityColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, String>("city"));
        postalCodeColumn.setCellValueFactory(new PropertyValueFactory<AdministrationEmployee, String>("postal_code"));
    }

    @FXML
    private void createDjelatnik(ActionEvent event) throws IOException {
        EntityManager entityManager = Main.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(

                new AdministrationEmployee(firstNameTextField.getText(), lastNameTextField.getText(),
                        usernameTextField.getText(),
                        passwordTextField.getText(), jmbgTextField.getText(),
                        new Date(),
                        addressTextField.getText(), cityTextField.getText(),
                        postalCodeTextField.getText(), oibTextField.getText(),"true"));

        entityManager.getTransaction().commit();
        entityManager.close();
        window = (Stage) createDjelatnik.getScene().getWindow();
        ManageScreens.switchToDjelatnikReferade(window);
    }

}
