package hr.tvz.naprednajava.refmanagementt3.controllers.student;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.entities.StudentCollegiumGrade;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by irusan on 7.7.2015..
 */
public class StudentOcjeneController implements Initializable {

    @FXML
    public TableView<StudentCollegiumGrade>  tableStudentScore;
    @FXML
    public TableColumn<StudentCollegiumGrade, String> collumnCollegiumName, collumnSemester;
    public TableColumn<StudentCollegiumGrade, Integer> collumnLabPoints, collumnColloquiumPoints, collumnExtraPoints,
            collumnTotalPoints, collumnGrade;

    @FXML
    public Button bttnMyNotices, bttnMyCollegiums, bttnMyGrades, bttnMyInformation, bttnLogout;

    @FXML
    private Label nameLabel;

    private Stage window;

    public StudentOcjeneController(){}

    @FXML
    public void RedirectMe(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnMyNotices) {
            window = (Stage) bttnMyNotices.getScene().getWindow();
            ManageScreens.switchToStudentObavijestiScreen(window);
        } else if (actionEvent.getSource() == bttnMyCollegiums) {
            window = (Stage) bttnMyCollegiums.getScene().getWindow();
            ManageScreens.switchToStudentKolegijiScreen(window);
        } else if (actionEvent.getSource() == bttnMyGrades) {
            window = (Stage) bttnMyGrades.getScene().getWindow();
            ManageScreens.switchToStudentOcjeneScreen(window);
        } else if (actionEvent.getSource() == bttnMyInformation) {
            window = (Stage) bttnMyInformation.getScene().getWindow();
            ManageScreens.switchToStudentPodaciScreen(window);
        } else if (actionEvent.getSource() == bttnLogout) {
            window = (Stage) bttnLogout.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            getStudentCollegiums();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    public void getStudentCollegiums() throws IOException {
        ObservableList<StudentCollegiumGrade> listaCollegiuma = FXCollections.observableList( RefRepository.getStudentPoints());

        collumnSemester.setCellValueFactory(new PropertyValueFactory<StudentCollegiumGrade, String>("semester"));
        collumnCollegiumName.setCellValueFactory(new PropertyValueFactory<StudentCollegiumGrade, String>("collegiumName"));
        collumnLabPoints.setCellValueFactory(new PropertyValueFactory<StudentCollegiumGrade, Integer>("labPoints"));
        collumnColloquiumPoints.setCellValueFactory(new PropertyValueFactory<StudentCollegiumGrade, Integer>("colloquiumPoints"));
        collumnExtraPoints.setCellValueFactory(new PropertyValueFactory<StudentCollegiumGrade, Integer>("extraPoints"));
        collumnTotalPoints.setCellValueFactory(new PropertyValueFactory<StudentCollegiumGrade, Integer>("totalPoints"));
        collumnGrade.setCellValueFactory(new PropertyValueFactory<StudentCollegiumGrade, Integer>("grade"));

        nameLabel.setText(CurrentUser.GetCurrentUser().getFullName());
        tableStudentScore.setItems(listaCollegiuma);

    }

    private void showStudentScore(Collegium collegium){

    }
}
