package hr.tvz.naprednajava.refmanagementt3.controllers.referada;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Student;
import hr.tvz.naprednajava.refmanagementt3.entities.Teacher;
import hr.tvz.naprednajava.refmanagementt3.entities.User;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.Main;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ReferadaNastavniciController implements Initializable {
    @FXML
    private TextField ime;
    @FXML
    private TextField dateOfBirth;
    @FXML
    private TextField mjesto;
    @FXML
    private Label djelatnikName;
    @FXML
    private TextField prezime;
    @FXML
    private TextField adresa;
    @FXML
    private TextField oib;
    @FXML
    private Button obavijesti;
    @FXML
    private Button kolegiji;
    @FXML
    private Button studenti;
    @FXML
    private Button nastavnici1;
    @FXML
    private Button podaci;
    @FXML
    private Button odjava;

    @FXML
    private TableColumn<Student, String> semestar;
    @FXML
    private TableColumn<Student, String> kolegij;
    @FXML
    private TableColumn<Student, String> labosi;
    @FXML
    private TableColumn<Student, String> kolokviji;
    @FXML
    private TableColumn<Student, String> ispit;
    @FXML
    private TableColumn<Student, String> dodatni;
    @FXML
    private TableColumn<Student, String> ukupno;
    @FXML
    private TableColumn<Student, String> ocjena;
    @FXML public TableView<Teacher> nastavnici;
    @FXML public TableColumn<Teacher, String> kolonaPrezimeNastavnika;
    @FXML public TableColumn<Teacher, String> kolonaImeNastavnika;
    @FXML
    public Label nameLabel;
    private Stage window;

    @FXML
    public Button bttnObavijesti, bttnKolegiji, bttnStudenti, bttnNastavnici, bttnPodaci, bttnOdjava;

    public ReferadaNastavniciController(){}

    public void initName() {
        User currentUser = CurrentUser.GetCurrentUser();

        String fullname = currentUser.getName();
        fullname += " " + currentUser.getSurname();
        nameLabel.setText(fullname);
    }

    @FXML
    public void redirect(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnObavijesti) {
            window = (Stage) bttnObavijesti.getScene().getWindow();
            ManageScreens.switchToReferadaObavijestiScreen(window);
        } else if (actionEvent.getSource() == bttnKolegiji) {
            window = (Stage) bttnKolegiji.getScene().getWindow();
            ManageScreens.switchToReferadaKolegijScreen(window);
        } else if (actionEvent.getSource() == bttnStudenti) {
            window = (Stage) bttnStudenti.getScene().getWindow();
            ManageScreens.switchToReferadaStudentiScreen(window);
        } else if (actionEvent.getSource() == bttnNastavnici) {
            window = (Stage) bttnNastavnici.getScene().getWindow();
            ManageScreens.switchToReferadaNastavniciScreen(window);
        } else if (actionEvent.getSource() == bttnPodaci) {
            window = (Stage) bttnPodaci.getScene().getWindow();
            ManageScreens.switchToReferadaPodaciScreen(window);
        } else if (actionEvent.getSource() == bttnOdjava) {
            window = (Stage) bttnOdjava.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            DohvatiListuNastavnika();
        } catch (IOException e) {
            e.printStackTrace();
        }
        disableInputFields(true);
        prikaziDetaljeNastavnika(null);
        initName();
        nastavnici.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> prikaziDetaljeNastavnika(newValue));
    }


    @FXML
    public void DohvatiListuNastavnika() throws IOException {
        ObservableList<Teacher> listaZaPrikaz = FXCollections.observableArrayList(RefRepository.getAllTeachers());

        kolonaImeNastavnika.setCellValueFactory(new PropertyValueFactory<Teacher, String>("name"));
        kolonaPrezimeNastavnika.setCellValueFactory(new PropertyValueFactory<Teacher, String>("surname"));
        nastavnici.setItems(listaZaPrikaz);
    }

    @FXML
    private void DodajNastavnika() throws IOException {
        Teacher tempTeacher = new Teacher();
        boolean okClicked = ManageScreens.switchToNastavnikAddEditScreen(tempTeacher, window);
        if (okClicked) {
            EntityManager entityManager = Main.getEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(tempTeacher);
            entityManager.getTransaction().commit();
            DohvatiListuNastavnika();
            entityManager.close();
        }
    }

    @FXML
    private void UrediNastavnika() throws IOException {
        Teacher odabraniNastavnik = nastavnici.getSelectionModel().getSelectedItem();
        if (odabraniNastavnik != null) {
            boolean okClicked = ManageScreens.switchToNastavnikAddEditScreen(odabraniNastavnik, window);
            if (okClicked) {
                EntityManager entityManager = Main.getEntityManager();
                entityManager.getTransaction().begin();
                entityManager.find(Teacher.class, odabraniNastavnik.getId());
                entityManager.merge(odabraniNastavnik);
                entityManager.getTransaction().commit();
                window = (Stage) bttnObavijesti.getScene().getWindow();
                ManageScreens.switchToReferadaNastavniciScreen(window);
                entityManager.close();
            }
        }
   }

    @FXML
    public void getTeachersAndFillTable() throws IOException {

        ObservableList<Teacher> listaZaPrikaz = FXCollections.observableArrayList(RefRepository.getAllTeachers());

        kolonaImeNastavnika.setCellValueFactory(new PropertyValueFactory<Teacher, String>("name"));
        kolonaPrezimeNastavnika.setCellValueFactory(new PropertyValueFactory<Teacher, String>("surname"));
        nastavnici.setItems(listaZaPrikaz);
    }

    @FXML
    private void IzbrisiNastavnika() throws IOException {
        disableInputFields(true);
        long selectedId = nastavnici.getSelectionModel().getSelectedItem().getId();
        EntityManager entityManager = Main.getEntityManager();
        entityManager.getTransaction().begin();
        Teacher teacher = entityManager.find(Teacher.class, selectedId);
        entityManager.remove(teacher);
        entityManager.getTransaction().commit();
        getTeachersAndFillTable();
        entityManager.close();
    }

    private void disableInputFields(boolean disable) {
        ime.setDisable(disable);
        prezime.setDisable(disable);
        adresa.setDisable(disable);
        dateOfBirth.setDisable(disable);
        mjesto.setDisable(disable);
        oib.setDisable(disable);
    }

    private void prikaziDetaljeNastavnika(Teacher teacher) {
        if (teacher != null) {
            ime.setText(teacher.getName());
            prezime.setText(teacher.getSurname());
            adresa.setText(teacher.getAddress());
            dateOfBirth.setText(teacher.getBirthday().toString());
            mjesto.setText(teacher.getCity());
            oib.setText(teacher.getJmbg());
        } else {
            ime.setText("");
            prezime.setText("");
            adresa.setText("");
            dateOfBirth.setText("");
            mjesto.setText("");
            oib.setText("");
        }
    }

}