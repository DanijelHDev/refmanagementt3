package hr.tvz.naprednajava.refmanagementt3.controllers.admin;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Cafuta;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AdminPodaciController implements Initializable {
    @FXML
    Label adminFullName;
    @FXML
    Button bttnAdministrationEmployees;
    @FXML
    Button bttnMyInformation;
    @FXML
    Button bttnLogout;
    @FXML
    Button bttnChangePassword;
    @FXML
    TextField oldPasswordTextField;
    @FXML
    TextField newPasswordTextField;
    @FXML
    TextField newPasswordReenterTextField;

    private Stage window;
    private Cafuta currentUser = new Cafuta();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentUser = (Cafuta) CurrentUser.GetCurrentUser();
        adminFullName.setText(currentUser.getName() + " " + currentUser.getSurname());
        bttnChangePassword.setDisable(false);
    }

    public AdminPodaciController(){}

    @FXML
    public void redirect(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnAdministrationEmployees) {
            window = (Stage) bttnAdministrationEmployees.getScene().getWindow();
            ManageScreens.switchToAdminDjelatniciScreen(window);

        } else if (actionEvent.getSource() == bttnMyInformation) {
            window = (Stage) bttnMyInformation.getScene().getWindow();
            ManageScreens.switchToAdminPodaciScreen(window);

        } else if (actionEvent.getSource() == bttnLogout) {
            window = (Stage) bttnLogout.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);

        }
    }

    @FXML
    public void changePassword(ActionEvent actionEvent) throws IOException {
        if (!isAnyFieldEmpty()) {
            if (newPasswordTextField.getText().equals(newPasswordReenterTextField.getText())
                    && oldPasswordTextField.getText().equals(currentUser.getPassword())) {

                currentUser.setPassword(newPasswordTextField.getText());
                RefRepository.updateUser(currentUser);

                window = (Stage) bttnAdministrationEmployees.getScene().getWindow();
                ManageScreens.switchToAdminDjelatniciScreen(window);
            } else {
                window = (Stage) bttnMyInformation.getScene().getWindow();
                ManageScreens.switchToMojiPodaciScreen(window);
            }
        }
    }

    private Boolean isAnyFieldEmpty() {
        return (oldPasswordTextField.getText().isEmpty() ||
                newPasswordTextField.getText().isEmpty() ||
                newPasswordReenterTextField.getText().isEmpty());
    }
}
