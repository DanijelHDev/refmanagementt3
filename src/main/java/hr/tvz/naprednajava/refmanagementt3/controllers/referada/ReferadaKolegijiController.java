package hr.tvz.naprednajava.refmanagementt3.controllers.referada;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.entities.Student;
import hr.tvz.naprednajava.refmanagementt3.entities.Teacher;
import hr.tvz.naprednajava.refmanagementt3.entities.User;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.Main;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Frane on 4.7.2015..
 */
public class ReferadaKolegijiController implements Initializable {

    @FXML Label nameLabel, LabelImeKolegija, LabelSemestar, LabelECTS, LabelOpis, LabelBrojStudenata, LabelNastavnik;
    @FXML ChoiceBox ChoiceBoxPromijeni;
    @FXML Button ButtonObavijesti, ButtonKolegiji, ButtonStudenti, ButtonNastavnici, ButtonMojiPodaci, ButtonOdjava, ButtonUredi, ButtonSpremi;
    @FXML TableView<Collegium> TableViewListaKolegija;
    @FXML TableView<Student> tablicaStudenata;
    @FXML TableColumn<Collegium, String> TableColumnKolegiji;
    @FXML TableColumn<Student, String> kolonaStudentIme, kolonaStudentPrezime, kolonaStudentJmbag;
    @FXML public Button bttnObavijesti, bttnKolegiji, bttnStudenti, bttnNastavnici, bttnPodaci, bttnOdjava, ButtonPotvrdi;
    private Stage window;

    public ReferadaKolegijiController(){}

    @FXML
    public void redirect(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnObavijesti) {
            window = (Stage) bttnObavijesti.getScene().getWindow();
            ManageScreens.switchToReferadaObavijestiScreen(window);
        } else if (actionEvent.getSource() == bttnKolegiji) {
            window = (Stage) bttnKolegiji.getScene().getWindow();
            ManageScreens.switchToReferadaKolegijScreen(window);
        } else if (actionEvent.getSource() == bttnStudenti) {
            window = (Stage) bttnStudenti.getScene().getWindow();
            ManageScreens.switchToReferadaStudentiScreen(window);
        } else if (actionEvent.getSource() == bttnNastavnici) {
            window = (Stage) bttnNastavnici.getScene().getWindow();
            ManageScreens.switchToReferadaNastavniciScreen(window);
        } else if (actionEvent.getSource() == bttnPodaci) {
            window = (Stage) bttnPodaci.getScene().getWindow();
            ManageScreens.switchToReferadaPodaciScreen(window);
        } else if (actionEvent.getSource() == bttnOdjava) {
            window = (Stage) bttnOdjava.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }
    public void initName() {
        User currentUser = CurrentUser.GetCurrentUser();

        String fullname = currentUser.getName();
        fullname += " " + currentUser.getSurname();
        nameLabel.setText(fullname);
    }
    @FXML
    public void RedirectMe(ActionEvent event) throws IOException {
        if (event.getSource() == ButtonObavijesti) {
            window = (Stage) ButtonObavijesti.getScene().getWindow();
            ManageScreens.switchToReferadaObavijestiScreen(window);
        } else if (event.getSource() == ButtonKolegiji) {
            window = (Stage) ButtonKolegiji.getScene().getWindow();
            ManageScreens.switchToReferadaKolegijScreen(window);
        } else if (event.getSource() == ButtonStudenti) {
            window = (Stage) ButtonStudenti.getScene().getWindow();
            ManageScreens.switchToReferadaStudentiScreen(window);
        } else if (event.getSource() == ButtonNastavnici) {
            window = (Stage) ButtonNastavnici.getScene().getWindow();
            ManageScreens.switchToReferadaNastavniciScreen(window);
        } else if (event.getSource() == ButtonMojiPodaci) {
            window = (Stage) ButtonMojiPodaci.getScene().getWindow();
            ManageScreens.switchToReferadaPodaciScreen(window);
        } else if (event.getSource() == ButtonOdjava) {
            window = (Stage) ButtonOdjava.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            DohvatiListuKolegija();
        } catch (IOException e) {
            e.printStackTrace();
        }
        initName();
        PrikaziDetaljeKolegija(null);
        TableViewListaKolegija.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {PrikaziDetaljeKolegija(newValue);
                    try {
                        PripremiPromjenuNastavnika();
                        DohvatiListuStudenata();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    @FXML
    public void DohvatiListuKolegija() throws IOException {
        List<Collegium> listaCollegiuma = RefRepository.getAllCollegiums();
        ObservableList<Collegium> listaZaPrikaz = FXCollections.observableArrayList(listaCollegiuma);

        TableColumnKolegiji.setCellValueFactory(new PropertyValueFactory<Collegium, String>("name"));
        TableViewListaKolegija.setItems(listaZaPrikaz);
    }

    @FXML
    public void DohvatiListuStudenata() throws IOException {
        Collegium kolegij = TableViewListaKolegija.getSelectionModel().getSelectedItem();

        List<Student> listaStudenata = kolegij.getStudents();
        ObservableList<Student> listaZaPrikaz = FXCollections.observableArrayList(listaStudenata);

        kolonaStudentIme.setCellValueFactory(new PropertyValueFactory<Student, String>("name"));
        kolonaStudentPrezime.setCellValueFactory(new PropertyValueFactory<Student, String>("surname"));
        kolonaStudentJmbag.setCellValueFactory(new PropertyValueFactory<Student, String>("jmbag"));

        int broj = listaStudenata.size();
        LabelBrojStudenata.setText(Integer.toString(broj));

        if(listaZaPrikaz.isEmpty()){

        } else{
            tablicaStudenata.setItems(listaZaPrikaz);
        }

    }

    @FXML
    public void PripremiPromjenuNastavnika() throws IOException {
        Collegium kolegij = TableViewListaKolegija.getSelectionModel().getSelectedItem();

        String ime = kolegij.getTeacher().getFullName();
        if (ime != null){
            LabelNastavnik.setText(ime);
        } else {
            LabelNastavnik.setText("Nema predavača.");
        }

        ObservableList<Teacher> listaNastavnika = FXCollections.observableArrayList(RefRepository.getAllTeachers());
        List<String> listaImenaNastavnika = listaNastavnika.stream().map(Teacher::getFullName).collect(Collectors.toList());
        ObservableList<String> listaZaPrikaz = FXCollections.observableArrayList(listaImenaNastavnika);

        ChoiceBoxPromijeni.setItems(listaZaPrikaz);
    }

    @FXML
    public void PromijeniNastavnika() throws IOException {
        Collegium kolegij = TableViewListaKolegija.getSelectionModel().getSelectedItem();
        String ime = ChoiceBoxPromijeni.getValue().toString();
        List<Teacher> listaNastavnika = RefRepository.getAllTeachers();
        List<Teacher> nastavnik = listaNastavnika.stream().filter(teacher -> teacher.getFullName().equals(ime)).collect(Collectors.toList());
        kolegij.setTeacher(nastavnik.get(0));
    }

    private void DodajKolegij(Collegium tempKolegij) throws IOException {
    }

    private void UrediKolegij(){
    }

    private void SpremiKolegij(){
    }

    private void IzbrisiKolegij() {
        Collegium kolegij = TableViewListaKolegija.getSelectionModel().getSelectedItem();
        RefRepository.deleteCollegium(kolegij);
    }

    private void PrikaziDetaljeKolegija(Collegium kolegij){
        if(kolegij != null){
            LabelImeKolegija.setText(kolegij.getName());
            LabelSemestar.setText(kolegij.getSemester());
            LabelECTS.setText(kolegij.getEcts());
            LabelOpis.setText(kolegij.getDescription());
        } else {
            LabelImeKolegija.setText("");
            LabelSemestar.setText("");
            LabelECTS.setText("");
            LabelOpis.setText("");
        }
    }

}
