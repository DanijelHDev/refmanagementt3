package hr.tvz.naprednajava.refmanagementt3.controllers.student;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Notice;
import hr.tvz.naprednajava.refmanagementt3.entities.User;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import hr.tvz.naprednajava.refmanagementt3.utils.NoticesUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class StudentObavijestiController implements Initializable {
    private final static Logger LOGGER = Logger.getLogger(StudentObavijestiController.class.getSimpleName());

    @FXML
    public Label nameLabel;
    @FXML
    public Button myNoticesButton;
    @FXML
    public VBox noticesContainerVBox;
    @FXML
    public VBox archivedNoticesContainerVBox;

    private List<Notice> noticeList;
    private Stage window;

    //Main meni
    @FXML private Button bttnMyCollegiums;
    @FXML private Button bttnMyGrades;
    @FXML private Button bttnMyNotices;
    @FXML private Button bttnMyInformation;
    @FXML private Button bttnLogout;

    @FXML
    private MenuItem bttnSwTheme;

    public StudentObavijestiController(){}

    @FXML
    public void RedirectMe(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnMyNotices) {
            window = (Stage) bttnMyNotices.getScene().getWindow();
            ManageScreens.switchToStudentObavijestiScreen(window);
        } else if (actionEvent.getSource() == bttnMyCollegiums) {
            window = (Stage) bttnMyCollegiums.getScene().getWindow();
            ManageScreens.switchToStudentKolegijiScreen(window);
        } else if (actionEvent.getSource() == bttnMyGrades) {
            window = (Stage) bttnMyGrades.getScene().getWindow();
            ManageScreens.switchToStudentOcjeneScreen(window);
        } else if (actionEvent.getSource() == bttnMyInformation) {
            window = (Stage) bttnMyInformation.getScene().getWindow();
            ManageScreens.switchToStudentPodaciScreen(window);
        } else if (actionEvent.getSource() == bttnLogout) {
            window = (Stage) bttnLogout.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }

    @FXML
    public void SwitchTheme(ActionEvent event) throws IOException {
        window = (Stage) bttnLogout.getScene().getWindow();
        ManageScreens.SwitchTheme();
        ManageScreens.switchToStudentObavijestiScreen(window);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LOGGER.info("StudentObavijestiController initialization started");
        initStudentName();
        retrieveNoticeList();
        initNumberOfNotices();
        initNoticeList();
        LOGGER.info("StudentObavijestiController initialization finished");
    }

    public void initStudentName() {
        User currentUser = CurrentUser.GetCurrentUser();

        String fullname = currentUser.getName();
        fullname += " " + currentUser.getSurname();

        LOGGER.info("User " + fullname + " entered notices screen");
        nameLabel.setText(fullname);
    }

    public void retrieveNoticeList() {
        noticeList = RefRepository.getAllNotices();
        if (noticeList != null) {
            LOGGER.info(noticeList.size() + " notices retrieved");
        } else {
            LOGGER.warning("Notice list is null");
        }
    }

    public void initNumberOfNotices() {
        NoticesUtils.initMyNoticesButton(bttnMyNotices, noticeList);
    }

    public void initNoticeList() {
        noticesContainerVBox.getChildren().clear();
        archivedNoticesContainerVBox.getChildren().clear();

        List<VBox> validNoticesWrapperList = new ArrayList<>();
        List<VBox> archivedNoticesWrapperList = new ArrayList<>();

        for (Notice n : noticeList) {
            VBox noticeWrapper = NoticesUtils.createNoticeVBox(n);
            if (n.isArhived()) {
                archivedNoticesWrapperList.add(noticeWrapper);
            } else {
                validNoticesWrapperList.add(noticeWrapper);
            }

        }
        noticesContainerVBox.getChildren().addAll(validNoticesWrapperList);
        archivedNoticesContainerVBox.getChildren().addAll(archivedNoticesWrapperList);
        LOGGER.info("Notice container populated");
    }

    public List<Notice> getNoticeList() {
        return noticeList;
    }
}
