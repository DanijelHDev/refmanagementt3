package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Frane on 19.6.2015..
 */
public class KolegijAddEditController implements Initializable {

    @FXML
    public TextField ime, ects, description;
    @FXML
    public ChoiceBox<String> semester;
    @FXML
    public Button buttSave, buttCancel;

    private Collegium collegium;
    private Stage dialogStage;
    private boolean okClicked = false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        semester.setValue("I");
        semester.setItems(FXCollections.observableArrayList("I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"));
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setKolegij(Collegium collegium) {
        this.collegium = collegium;

        ime.setText(collegium.getName());
        semester.setValue(collegium.getSemester());
        ects.setText(collegium.getEcts());
        description.setText(collegium.getDescription());
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void Save() {
        collegium.setName(ime.getText());
        collegium.setSemester(semester.getValue());
        collegium.setEcts(ects.getText());
        collegium.setDescription(description.getText());

        okClicked = true;
        dialogStage.close();
    }

    @FXML
    private void Cancel() {
        dialogStage.close();
    }
}
