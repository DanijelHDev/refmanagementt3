package hr.tvz.naprednajava.refmanagementt3.controllers.referada;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.AdministrationEmployee;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ReferadaPodaciController implements Initializable {
    @FXML
    private Label fullNameLabel;
    @FXML
    private Button bttnMyNotices;
    @FXML
    private Button bttnSubjects;
    @FXML
    private Button bttnStudents;
    @FXML
    private Button bttnProfesors;
    @FXML
    private Button bttnMyInformation;
    @FXML
    private Button bttnLogout;
    @FXML
    private Button bttnChangePassword;
    @FXML
    private TextField oldPasswordTextField;
    @FXML
    private TextField newPasswordTextField;
    @FXML
    private TextField newPasswordReenterTextField;

    @FXML
    public Button bttnObavijesti, bttnKolegiji, bttnStudenti, bttnNastavnici, bttnPodaci, bttnOdjava;

    private Stage window;
    private AdministrationEmployee currentUser = new AdministrationEmployee();

    public ReferadaPodaciController(){}

    @FXML
    public void redirect(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnObavijesti) {
            window = (Stage) bttnObavijesti.getScene().getWindow();
            ManageScreens.switchToReferadaObavijestiScreen(window);
        } else if (actionEvent.getSource() == bttnKolegiji) {
            window = (Stage) bttnKolegiji.getScene().getWindow();
            ManageScreens.switchToReferadaKolegijScreen(window);
        } else if (actionEvent.getSource() == bttnStudenti) {
            window = (Stage) bttnStudenti.getScene().getWindow();
            ManageScreens.switchToReferadaStudentiScreen(window);
        } else if (actionEvent.getSource() == bttnNastavnici) {
            window = (Stage) bttnNastavnici.getScene().getWindow();
            ManageScreens.switchToReferadaNastavniciScreen(window);
        } else if (actionEvent.getSource() == bttnPodaci) {
            window = (Stage) bttnPodaci.getScene().getWindow();
            ManageScreens.switchToReferadaPodaciScreen(window);
        } else if (actionEvent.getSource() == bttnOdjava) {
            window = (Stage) bttnOdjava.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentUser = (AdministrationEmployee) CurrentUser.GetCurrentUser();
        fullNameLabel.setText(currentUser.getName() + " " + currentUser.getSurname());
    }

    @FXML
    public void changePassword(ActionEvent actionEvent) throws IOException {
        if (!isAnyFieldEmpty()) {
            if (newPasswordTextField.getText().equals(newPasswordReenterTextField.getText())
                    && oldPasswordTextField.getText().equals(currentUser.getPassword())) {

                currentUser.setPassword(newPasswordTextField.getText());
                RefRepository.updateUser(currentUser);

                window = (Stage) bttnObavijesti.getScene().getWindow();
                ManageScreens.switchToReferadaObavijestiScreen(window);
            } else {
                window = (Stage) bttnPodaci.getScene().getWindow();
                ManageScreens.switchToReferadaPodaciScreen(window);
            }
        }
    }

    private Boolean isAnyFieldEmpty() {
        return (oldPasswordTextField.getText().isEmpty() ||
                newPasswordTextField.getText().isEmpty() ||
                newPasswordReenterTextField.getText().isEmpty());
    }
}
