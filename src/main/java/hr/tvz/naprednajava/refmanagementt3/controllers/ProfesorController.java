package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.entities.Student;
import hr.tvz.naprednajava.refmanagementt3.entities.Teacher;
import hr.tvz.naprednajava.refmanagementt3.main.Main;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.awt.Label;
import java.io.IOException;


/**
 * Created by Danijel on 19.5.2015..
 */
public class ProfesorController {

    @FXML
    private Button buttPovratak;
    @FXML
    Button buttMojiPodaci;
    @FXML
    Button buttObavijesti;
    @FXML
    private MenuItem buttOdjava;
    @FXML
    private MenuItem buttTheme;

    @FXML
    private TableView<Teacher> tablicaStudenata;
    @FXML
    private TableColumn<Teacher, String> imeStudenta;
    @FXML
    private TableColumn<Teacher, String> prezimeStudenta;

    @FXML
    private ObservableList<Teacher> studentData;

    @FXML
    private Label imeLabel;
    @FXML
    private TextField ocjenaStudenta;

    private Stage window;

    @FXML
    public void RedirectMe(ActionEvent event) throws IOException {
        if (event.getSource() == buttPovratak) {
            window = (Stage) buttPovratak.getScene().getWindow();
            ManageScreens.switchToWelcomeScreen(window);
        } else if (event.getSource() == buttMojiPodaci) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToMojiPodaciScreen(window);
        } else if (event.getSource() == buttObavijesti) {
            window = (Stage) buttObavijesti.getScene().getWindow();
            ManageScreens.switchToObavijestiScreen(window);
        } else if (event.getSource() == buttOdjava) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }

    @FXML
    public void SwitchTheme(ActionEvent event) throws IOException {
        window = (Stage) buttMojiPodaci.getScene().getWindow();
        ManageScreens.SwitchTheme();
        ManageScreens.switchToObavijestiScreen(window);
    }

    @FXML
    public void initialize() {
        getStudentList();
        imeStudenta.setCellValueFactory(new PropertyValueFactory<Teacher, String>("name"));
        prezimeStudenta.setCellValueFactory(new PropertyValueFactory<Teacher, String>("surname"));
    }

    @FXML
    public void getStudentList() {
        EntityManager entityManager = Main.getEntityManager();
        entityManager.getTransaction().begin();
        studentData = FXCollections.observableArrayList(entityManager.createQuery("from Profesor", Teacher.class).getResultList());
        entityManager.getTransaction().commit();
        entityManager.close();
        tablicaStudenata.setItems(studentData);
    }

    private void unesiOcijenu(Student stud) {
        if (stud != null) {
            imeLabel.setText(stud.getName());
        } else {
            imeLabel.setText("");
        }
    }
}
