package hr.tvz.naprednajava.refmanagementt3.controllers.nastavnik;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.entities.Notice;
import hr.tvz.naprednajava.refmanagementt3.entities.User;
import hr.tvz.naprednajava.refmanagementt3.listeners.OnNoticeArchivedListener;
import hr.tvz.naprednajava.refmanagementt3.listeners.OnNoticeDeletedListener;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import hr.tvz.naprednajava.refmanagementt3.utils.NoticesUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Created by etombla on 4.7.2015..
 */
public class NastavnikObavijestiController implements Initializable, OnNoticeDeletedListener {
    private final static Logger LOGGER = Logger.getLogger(NastavnikObavijestiController.class.getSimpleName());

    @FXML
    public Label nameLabel;
    @FXML
    public VBox noticesContainerVBox;
    @FXML
    public VBox archivedNoticesContainerVBox;

    @FXML
    public ComboBox<String> collegiumComboBox;
    @FXML
    public TextField noticeTitleTextField;
    @FXML
    public TextArea noticeBodyTextArea;
    @FXML
    public DatePicker expirationDatePicker;
    @FXML
    private Button bttnMyNotices;
    @FXML
    private Button bttnMySubjects;
    @FXML
    private Button bttnMyInformation;
    @FXML
    private Button bttnLogout;

    private List<Notice> noticeList;
    private List<Collegium> collegiumsList;
    private Stage window;

    public NastavnikObavijestiController(){}

    @FXML
    public void redirect(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnMyNotices) {
            window = (Stage) bttnMyNotices.getScene().getWindow();
            ManageScreens.switchToNastavnikObavijestiScreen(window);

        } else if (actionEvent.getSource() == bttnMySubjects) {
            window = (Stage) bttnMySubjects.getScene().getWindow();
            ManageScreens.switchToNastavnikKolegijiScreen(window);

        } else if (actionEvent.getSource() == bttnMyInformation) {
            window = (Stage) bttnMyInformation.getScene().getWindow();
            ManageScreens.switchToNastavnikPodaciScreen(window);

        } else if (actionEvent.getSource() == bttnLogout) {
            window = (Stage) bttnLogout.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);

        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LOGGER.info("NastavnikObavijestiController initialization started");
        initStudentName();
        retrieveNoticeList();
        retrieveCollegiumsList();
        initNoticeList();
        NoticesUtils.initCollegiumComboBox(collegiumComboBox, collegiumsList);
        LOGGER.info("NastavnikObavijestiController initialization finished");
    }

    public void initStudentName() {
        User currentUser = CurrentUser.GetCurrentUser();

        String fullname = currentUser.getName();
        fullname += " " + currentUser.getSurname();

        LOGGER.info("User " + fullname + " entered notices screen");
        nameLabel.setText(fullname);
    }

    public void retrieveNoticeList() {
        noticeList = RefRepository.getAllNotices();
        LOGGER.info(noticeList.size() + " notices retrieved");
    }

    public void retrieveCollegiumsList() {
        collegiumsList = RefRepository.getAllCollegiumsForCurrentUser();
        LOGGER.info(collegiumsList.size() + " collegiums retrieved");
    }

    public void initNoticeList() {
        noticesContainerVBox.getChildren().clear();
        archivedNoticesContainerVBox.getChildren().clear();

        List<VBox> validNoticesWrapperList = new ArrayList<>();
        List<VBox> archivedNoticesWrapperList = new ArrayList<>();

        for (Notice n : noticeList) {
            VBox noticeWrapper = NoticesUtils.createNoticeAdminVBox(n, this);
            if (n.isArhived()) {
                archivedNoticesWrapperList.add(noticeWrapper);
            } else {
                validNoticesWrapperList.add(noticeWrapper);
            }

        }
        noticesContainerVBox.getChildren().addAll(validNoticesWrapperList);
        archivedNoticesContainerVBox.getChildren().addAll(archivedNoticesWrapperList);
        LOGGER.info("Notice container populated");
    }

    @Override
    public void onNoticeDeleted(Notice notice) {
        noticeList.remove(notice);
        RefRepository.deleteNotice(notice);
        LOGGER.info("Notice" + notice.getId() + " deleted");

        try {
            ManageScreens.switchToNastavnikObavijestiScreen((Stage) nameLabel.getScene().getWindow());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void submitNotice() {
        User user = CurrentUser.GetCurrentUser();
        Collegium collegium = NoticesUtils.getCollegiumFromComboBox(collegiumComboBox, collegiumsList);
        String title = noticeTitleTextField.getText();
        String body = noticeBodyTextArea.getText();
        Date dateCreated = new Date();

        LocalDate localDate = expirationDatePicker.getValue();
        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
        Date expirationDate = Date.from(instant);

        Notice notice = new Notice(user, collegium, title, body, dateCreated, expirationDate);
        RefRepository.insertNotice(notice);
        LOGGER.info("Notice" + notice.getId() + " created");

        try {
            ManageScreens.switchToNastavnikObavijestiScreen((Stage) nameLabel.getScene().getWindow());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Notice> getNoticeList() {
        return noticeList;
    }

    public List<Collegium> getCollegiumsList() {
        return collegiumsList;
    }
}
