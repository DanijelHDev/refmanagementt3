package hr.tvz.naprednajava.refmanagementt3.controllers.nastavnik;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Teacher;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class NastavnikPodaciController implements Initializable {
    @FXML
    private Label nastavnikFullNameLabel;
    @FXML
    private Button bttnMyNotices;
    @FXML
    private Button bttnMySubjects;
    @FXML
    private Button bttnMyInformation;
    @FXML
    private Button bttnLogout;
    /*@FXML
    private Button bttnChangePassword;*/
    @FXML
    private TextField oldPasswordTextField;
    @FXML
    private TextField newPasswordTextField;
    @FXML
    private TextField newPasswordReenterTextField;

    private Stage window;
    private Teacher currentUser = new Teacher();

    public NastavnikPodaciController(){}

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentUser = (Teacher) CurrentUser.GetCurrentUser();
        nastavnikFullNameLabel.setText(currentUser.getName() + " " + currentUser.getSurname());
    }

    @FXML
    public void redirect(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == bttnMyNotices) {
            window = (Stage) bttnMyNotices.getScene().getWindow();
            ManageScreens.switchToNastavnikObavijestiScreen(window);

        } else if (actionEvent.getSource() == bttnMySubjects) {
            window = (Stage) bttnMySubjects.getScene().getWindow();
            ManageScreens.switchToNastavnikKolegijiScreen(window);

        } else if (actionEvent.getSource() == bttnMyInformation) {
            window = (Stage) bttnMyInformation.getScene().getWindow();
            ManageScreens.switchToNastavnikPodaciScreen(window);

        } else if (actionEvent.getSource() == bttnLogout) {
            window = (Stage) bttnLogout.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);

        }
    }

    @FXML
    public void changePassword(ActionEvent actionEvent) throws IOException {
        if (!isAnyFieldEmpty()) {
            if (newPasswordTextField.getText().equals(newPasswordReenterTextField.getText())
                    && oldPasswordTextField.getText().equals(currentUser.getPassword())) {

                currentUser.setPassword(newPasswordTextField.getText());
                RefRepository.updateUser(currentUser);

                window = (Stage) bttnMyNotices.getScene().getWindow();
                ManageScreens.switchToNastavnikObavijestiScreen(window);
            } else {
                window = (Stage) bttnMyInformation.getScene().getWindow();
                ManageScreens.switchToMojiPodaciScreen(window);
            }
        }
    }

    public Boolean isAnyFieldEmpty() {
        return (oldPasswordTextField.getText().isEmpty() ||
                newPasswordTextField.getText().isEmpty() ||
                newPasswordReenterTextField.getText().isEmpty());
    }
}
