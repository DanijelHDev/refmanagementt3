package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.main.Main;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;

/**
 * Created by Danijel on 16.5.2015..
 */
public class WelcomeController implements Initializable {

    @FXML
    private Button buttMojiPodaci;
    @FXML
    private Button buttObavijesti;
    @FXML
    private Button buttKolegiji;
    @FXML
    private Button buttStudenti;
    @FXML
    private Button buttProfesori;
    @FXML
    private Button buttCafuta;
    @FXML
    private Button buttDjelatnik;
    @FXML
    private MenuItem buttOdjava;
    @FXML
    private MenuItem buttTheme;
    @FXML
    private Label lblWelcomeMsg;
    @FXML
    Parent root;
    private Stage window;

    @FXML
    public void RedirectMe(ActionEvent event) throws IOException {
        if (event.getSource() == buttMojiPodaci) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToMojiPodaciScreen(window);
        } else if (event.getSource() == buttObavijesti) {
            window = (Stage) buttObavijesti.getScene().getWindow();
            ManageScreens.switchToObavijestiScreen(window);
        } else if (event.getSource() == buttCafuta) {
            window = (Stage) buttCafuta.getScene().getWindow();
            ManageScreens.switchToAdministratorScreen(window);
        } else if (event.getSource() == buttKolegiji) {
            window = (Stage) buttKolegiji.getScene().getWindow();
            ManageScreens.switchToListaKolegijaScreen(window);
        } else if (event.getSource() == buttProfesori) {
            window = (Stage) buttProfesori.getScene().getWindow();
            ManageScreens.switchToProfesorScreen(window);
        } else if (event.getSource() == buttStudenti) {
            window = (Stage) buttStudenti.getScene().getWindow();
            ManageScreens.switchToStudentScreen(window);
        } else if (event.getSource() == buttDjelatnik) {
            window = (Stage) buttDjelatnik.getScene().getWindow();
            ManageScreens.switchToDjelatnikReferade(window);
        } else if (event.getSource() == buttOdjava) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToLoginScreen(window);
        }
    }

    @Override
    public void initialize(java.net.URL location, java.util.ResourceBundle resources) {
        EntityManager entityManager = Main.getEntityManager();
        long count = (long) entityManager.createQuery("SELECT COUNT(e) FROM Notice e").getSingleResult();
        entityManager.close();
        lblWelcomeMsg.setText("Dobrodošao {ImeKorisnika}, trenutno imate " + count + " obavijesti!");
    }


    @FXML
    public void SwitchTheme(ActionEvent event) throws IOException {
        window = (Stage) buttMojiPodaci.getScene().getWindow();
        ManageScreens.SwitchTheme();
        ManageScreens.switchToWelcomeScreen(window);
    }

}
