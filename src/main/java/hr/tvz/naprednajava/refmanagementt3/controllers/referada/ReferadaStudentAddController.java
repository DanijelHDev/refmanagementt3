package hr.tvz.naprednajava.refmanagementt3.controllers.referada;

import hr.tvz.naprednajava.refmanagementt3.entities.Student;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created by Frane on 11.7.2015..
 */
public class ReferadaStudentAddController implements Initializable {

    @FXML private TextField firstNameTextField, secondNameTextField, addressTextField, cityTextField, postalCodeTextField, jmbgTextField, jmbagTextField, usernameTextField, passwordTextField;

    @FXML DatePicker birthdayDatePicker;

    @FXML Button buttSave, buttCancel;


    private Stage dialogStage;
    private boolean okClicked = false;

    public ReferadaStudentAddController(){}


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    @FXML
    public Student Save() {
        Student student = new Student();
        student.setName(firstNameTextField.getText());
        student.setSurname(secondNameTextField.getText());
        LocalDate localDate = birthdayDatePicker.getValue();
        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
        Date birthday = Date.from(instant);
        student.setBirthday(birthday);
        student.setAddress(addressTextField.getText());
        student.setCity(cityTextField.getText());
        student.setPostalCode(postalCodeTextField.getText());
        student.setJmbg(jmbgTextField.getText());
        student.setJmbag(jmbagTextField.getText());
        student.setUsername(usernameTextField.getText());
        student.setPassword(passwordTextField.getText());

        okClicked = true;
        dialogStage.close();

        return student;
    }

    @FXML
    private void Cancel() {
        dialogStage.close();
    }
}
