package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.bil.CollegiumDaoImpl;
import hr.tvz.naprednajava.refmanagementt3.bil.exceptions.DAOException;
import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.main.Main;
import hr.tvz.naprednajava.refmanagementt3.main.ManageScreens;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Created by Danijel on 19.5.2015..
 */
public class KolegijController implements Initializable {
    private CollegiumDaoImpl collegiumDao;

    @FXML
    public Label labelIme, labelSemestar, labelECTS, labelOpis;
    @FXML
    public TableView<Collegium> tablicaKolegija;
    @FXML
    public TableColumn<Collegium, String> kolonaImeKolegija;
    @FXML
    private Button buttPovratak, buttMojiPodaci, buttObavijesti, buttEdit, buttAdd, buttDelete;
    @FXML
    private MenuItem buttOdjava, buttTheme;

    private Stage window;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            DohvatiListuKolegija();
        } catch (IOException e) {
            e.printStackTrace();
        }
        prikaziDetaljeKolegija(null);
        tablicaKolegija.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> prikaziDetaljeKolegija(newValue));
    }

    @FXML
    public void RedirectMe(ActionEvent event) throws IOException {
        if (event.getSource() == buttPovratak) {
            window = (Stage) buttPovratak.getScene().getWindow();
            ManageScreens.switchToWelcomeScreen(window);
        } else if (event.getSource() == buttMojiPodaci) {
            window = (Stage) buttMojiPodaci.getScene().getWindow();
            ManageScreens.switchToMojiPodaciScreen(window);
        } else if (event.getSource() == buttObavijesti) {
            window = (Stage) buttObavijesti.getScene().getWindow();
            ManageScreens.switchToObavijestiScreen(window);
        }
    }

    @FXML
    public void DohvatiListuKolegija() throws IOException {
        try{
            collegiumDao.registerEntityManager(Main.getEntityManager());
        }catch (DAOException e){

        }

        List<Collegium> listaKolegija = new List<Collegium>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Collegium> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Collegium kolegij) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Collegium> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends Collegium> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public Collegium get(int index) {
                return null;
            }

            @Override
            public Collegium set(int index, Collegium element) {
                return null;
            }

            @Override
            public void add(int index, Collegium element) {

            }

            @Override
            public Collegium remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<Collegium> listIterator() {
                return null;
            }

            @Override
            public ListIterator<Collegium> listIterator(int index) {
                return null;
            }

            @Override
            public List<Collegium> subList(int fromIndex, int toIndex) {
                return null;
            }
        };

        List<Collegium> collegiumList = collegiumDao.getAllFromDB();
        ObservableList<Collegium> listaZaPrikaz = FXCollections.observableArrayList(collegiumList);

        kolonaImeKolegija.setCellValueFactory(new PropertyValueFactory<Collegium, String>("name"));
        tablicaKolegija.setItems(listaZaPrikaz);
    }

    @FXML
    private void DodajKolegij() throws IOException {
        Collegium tempKolegij = new Collegium();
        boolean okClicked = ManageScreens.switchToKolegijAddEditScreen(tempKolegij, window);
        if (okClicked) {
            EntityManager entityManager = Main.getEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(tempKolegij);
            entityManager.getTransaction().commit();
            entityManager.close();
            DohvatiListuKolegija();
        }
    }

    @FXML
    private void UrediKolegij() throws IOException {
        Collegium odabraniKolegij = tablicaKolegija.getSelectionModel().getSelectedItem();
        if (odabraniKolegij != null) {
            boolean okClicked = ManageScreens.switchToKolegijAddEditScreen(odabraniKolegij, window);
            if (okClicked) {
                EntityManager entityManager = Main.getEntityManager();
                entityManager.getTransaction().begin();
                entityManager.find(Collegium.class, odabraniKolegij.getId());
                entityManager.merge(odabraniKolegij);
                entityManager.getTransaction().commit();
                entityManager.close();
                DohvatiListuKolegija();
            }
        }
    }

    @FXML
    private void IzbrisiKolegij() throws IOException {
        long selectedId = tablicaKolegija.getSelectionModel().getSelectedItem().getId();
        EntityManager entityManager = Main.getEntityManager();
        entityManager.getTransaction().begin();
        Collegium kolegij = entityManager.find(Collegium.class, selectedId);
        entityManager.remove(kolegij);
        entityManager.getTransaction().commit();
        entityManager.close();
        DohvatiListuKolegija();
    }

    private void prikaziDetaljeKolegija(Collegium kolegij) {
        if (kolegij != null) {
            labelIme.setText(kolegij.getName());
            labelSemestar.setText(kolegij.getSemester());
            labelECTS.setText(kolegij.getEcts());
            labelOpis.setText(kolegij.getDescription());
        } else {
            labelIme.setText("");
            labelSemestar.setText("");
            labelECTS.setText("");
            labelOpis.setText("");
        }
    }

    public void SwitchTheme(ActionEvent event) throws IOException {
        window = (Stage) buttMojiPodaci.getScene().getWindow();
        ManageScreens.SwitchTheme();
        ManageScreens.switchToListaKolegijaScreen(window);
    }
}
