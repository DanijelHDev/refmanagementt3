package hr.tvz.naprednajava.refmanagementt3.main;

import hr.tvz.naprednajava.refmanagementt3.controllers.KolegijAddEditController;
import hr.tvz.naprednajava.refmanagementt3.controllers.nastavnik.NastavnikAddEditController;
import hr.tvz.naprednajava.refmanagementt3.controllers.referada.ReferadaStudentAddController;
import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.entities.Student;
import hr.tvz.naprednajava.refmanagementt3.entities.Teacher;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Danijel on 16.5.2015..
 */
public class ManageScreens {
    private static Scene scene;
    private static String currentTheme = "stylesheets/styleLight.css";
    private static boolean defTheme = false;

    public static void switchToLoginScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/LoginScreen.fxml"));
        currentStage.close();
        scene = new Scene(parent, 500, 500);
        setStylesheet();
        currentStage.setTitle("Login");
        currentStage.setScene(scene);
        currentStage.show();
    }

    //Moji podaci
    /* -------------------------------------------------------------------------------------------- */
    public static void switchToMojiPodaciScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/MojiPodaci.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("Moji podaci");
        currentStage.setScene(scene);
        currentStage.show();
    }
    /* -------------------------------------------------------------------------------------------- */

    //admin
    /* -------------------------------------------------------------------------------------------- */

    public static void switchToAdminDjelatniciScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Admin/AdminDjelatnici.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("admin - Administracija djelatnika");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToAdminPodaciScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Admin/AdminPodaci.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("admin - Administracija podataka");
        currentStage.setScene(scene);
        currentStage.show();
    }

    /* -------------------------------------------------------------------------------------------- */

    //nastavnik
    /* -------------------------------------------------------------------------------------------- */
    public static void switchToNastavnikKolegijiScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Nastavnik/NastavnikKolegiji.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("nastavnik - Kolegiji");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static boolean switchToNastavnikAddEditScreen(Teacher teacher, Stage currentStage) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ManageScreens.class.getResource("/screens/Nastavnik/NastavnikAddEdit.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Dodaj ili uredi nastavnika");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(currentStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            NastavnikAddEditController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setNastavnik(teacher);

            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void switchToNastavnikObavijestiScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Nastavnik/NastavnikObavijesti.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("nastavnik - Obavijesti");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToNastavnikPodaciScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Nastavnik/NastavnikPodaci.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("nastavnik - Podaci");
        currentStage.setScene(scene);
        currentStage.show();
    }
    /* -------------------------------------------------------------------------------------------- */


    //referada
    /* -------------------------------------------------------------------------------------------- */
    public static void switchToReferadaKolegijScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Referada/ReferadaKolegiji.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("referada - Kolegiji");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static boolean switchToAddStudent(Stage currentStage) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ManageScreens.class.getResource("/screens/Referada/CreateStudents.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Dodaj ili uredi nastavnika");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(currentStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            NastavnikAddEditController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void switchToReferadaNastavniciScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Referada/ReferadaNastavnici.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("referada - Nastavnici");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToReferadaObavijestiScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Referada/ReferadaObavijesti.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("referada - Obavijesti");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToReferadaPodaciScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Referada/ReferadaPodaci.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("referada - Podaci");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToReferadaStudentiScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Referada/ReferadaStudenti.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("referada - Studenti");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static Student switchToReferadaStudentAdd(Stage currentStage) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ManageScreens.class.getResource("/screens/Referada/ReferadaStudentAdd.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Dodaj ili uredi studenta");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(currentStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ReferadaStudentAddController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            dialogStage.showAndWait();

            return controller.Save();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    /* -------------------------------------------------------------------------------------------- */


    //student
    /* -------------------------------------------------------------------------------------------- */
    public static void switchToStudentKolegijiScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Student/StudentKolegiji.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("student - Kolegiji");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToStudentObavijestiScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Student/StudentObavijesti.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("student - Obavijesti");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToStudentOcjeneScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Student/StudentOcjene.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("student - Ocjene");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToStudentPodaciScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Student/StudentPodaci.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("student - Podaci");
        currentStage.setScene(scene);
        currentStage.show();
    }
    /* -------------------------------------------------------------------------------------------- */


    //Deprecated
    /* -------------------------------------------------------------------------------------------- */
    public static void switchToObavijestiScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/notices/Notice.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("Obavijesti");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToListaKolegijaScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/kolegiji/Kolegij.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("Moji kolegiji");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static boolean switchToKolegijAddEditScreen(Collegium kolegij, Stage currentStage) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ManageScreens.class.getResource("/screens/kolegiji/KolegijAddEdit.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Dodaj ili uredi kolegij");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(currentStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            KolegijAddEditController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setKolegij(kolegij);

            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    //Deprecated ekran, obrisat ćemo
    public static void switchToWelcomeScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/WelcomeScreen.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("Teacher");
        currentStage.setScene(scene);
        currentStage.show();
    }

    //deprecated, obrisat ćemo
    public static void switchToStudentScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/students/Student.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("student");
        currentStage.setScene(scene);
        currentStage.show();
    }

    //deprecated, obrisat ćemo
    public static void switchToDjelatnikReferade(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/djelatnik/Djelatnik.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("AdministrationEmployee referade - Home");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToKreirajStudentaScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/students/createStudents.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("Kreiraj studenta");
        currentStage.setScene(scene);
        currentStage.show();
    }

    //deprecated, obrisat ćemo
    public static void switchToNoticeNewScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/notices/NoticeNew.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("Notice");
        currentStage.setScene(scene);
        currentStage.show();
    }

    //deprecated, obrisat ćemo
    public static void switchToAdministratorScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/administrator/Cafuta.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("Administrator - Home");
        currentStage.setScene(scene);
        currentStage.show();
    }

    //deprecated, obrisat ćemo
    public static void switchToCreateAdminScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/administrator/createAdministrator.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("Administrator - Create");
        currentStage.setScene(scene);
        currentStage.show();
    }

    //deprecated, obrisat ćemo
    public static void switchToCreateDjelatnikScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/djelatnik/createDjelatnik.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("AdministrationEmployee - Create");
        currentStage.setScene(scene);
        currentStage.show();
    }

    public static void switchToProfesorScreen(Stage currentStage) throws IOException {
        Parent parent = FXMLLoader.load(ManageScreens.class.getResource("/screens/Profesor.fxml"));
        scene = new Scene(parent, 1150, 700);
        setStylesheet();
        currentStage.setTitle("Profesor");
        currentStage.setScene(scene);
        currentStage.show();
    }

    /* -------------------------------------------------------------------------------------------- */


    public static void SwitchTheme() throws IOException {
        if (defTheme) {
            currentTheme = "stylesheets/styleLight.css";
            defTheme = false;
        } else {
            currentTheme = "stylesheets/style.css";
            defTheme = true;
        }
    }

    private static void setStylesheet() {
        scene.getStylesheets().add(currentTheme);
    }
}
