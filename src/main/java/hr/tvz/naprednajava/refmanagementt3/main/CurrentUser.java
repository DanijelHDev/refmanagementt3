package hr.tvz.naprednajava.refmanagementt3.main;

import hr.tvz.naprednajava.refmanagementt3.entities.*;

/**
 * Created by Danijel on 3.7.2015..
 */
public class CurrentUser {

    private static User currentUser;
    private static String typeOfUser;

    public static void SetCurrentUser(User model) {
        if (model instanceof Student) {
            currentUser = new Student();
            currentUser = model;
            typeOfUser = "student";
        } else if (model instanceof Teacher) {
            currentUser = new Teacher();
            currentUser = model;
            typeOfUser = "Profesor";
        } else if (model instanceof AdministrationEmployee) {
            currentUser = new AdministrationEmployee();
            currentUser = model;
            typeOfUser = "Djelatnik";
        } else {
            currentUser = new Cafuta();
            currentUser = model;
            typeOfUser = "admin(Cafuta)";
        }
    }

    public static User GetCurrentUser() {
        return currentUser;
    }

    public static String getTypeOfUser() {
        return typeOfUser;
    }

    public static void setTypeOfUser(String typeOfUser) {
        CurrentUser.typeOfUser = typeOfUser;
    }
}
