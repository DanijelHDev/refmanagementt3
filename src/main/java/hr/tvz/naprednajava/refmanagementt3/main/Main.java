package hr.tvz.naprednajava.refmanagementt3.main;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.utils.logging.LoggerConfiguration;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.logging.Logger;

public class Main extends Application {
    private final static Logger LOGGER = Logger.getLogger(Main.class.getSimpleName());

    private static EntityManager entityManager;
    private static RefRepository repository;

    @Override
    public void start(Stage primaryStage) throws Exception {
/*        setupLogging();*/
        EntityManagerFactory entityManagerFactory;

        LOGGER.info("Initializing database connection");
        entityManagerFactory = Persistence.createEntityManagerFactory("mazure");
        LOGGER.info("Database connected");

        entityManager =  entityManagerFactory.createEntityManager();
        RefRepository.initializeManager();

        LOGGER.info("Presenting login screen");
        Parent root = FXMLLoader.load(getClass().getResource("/screens/LoginScreen.fxml"));
        primaryStage.setTitle("Login screen");
        Scene scene = new Scene(root, 500, 500);
        scene.getStylesheets().add("stylesheets/styleLight.css");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void setupLogging() {
        try {
            LoggerConfiguration.setup();
        } catch (IOException e) {
            LOGGER.severe("Problems with creating the log files");
            LOGGER.severe(e.getMessage());
            throw new RuntimeException("Problems with creating the log files");
        }
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
