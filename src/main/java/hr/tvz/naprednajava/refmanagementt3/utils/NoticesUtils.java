package hr.tvz.naprednajava.refmanagementt3.utils;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.entities.Notice;
import hr.tvz.naprednajava.refmanagementt3.listeners.OnNoticeArchivedListener;
import hr.tvz.naprednajava.refmanagementt3.listeners.OnNoticeDeletedListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by etombla on 3.7.2015..
 */
public class NoticesUtils {
    public static VBox createNoticeVBox(Notice notice) {
        VBox noticeBox = new VBox();

        // Set all the values for the notice
        Label noticeDate = new Label();
        noticeDate.setText(notice.getDateCreated().toString());
        Label noticeCollegium = new Label();
        noticeCollegium.setText("Kolegij: " + notice.getCollegium().getName());
        Label noticeSubmitter = new Label();
        noticeSubmitter.setText("Objavio/la: " + notice.getUser().getName());
        Label noticeTitle = new Label();
        noticeTitle.setText(notice.getTitle());
        Label noticeBody = new Label();
        noticeBody.setText(notice.getBody());

        // Set bottom padding for all labels
        noticeDate.setPadding(new Insets(0, 0, 5, 0));
        noticeSubmitter.setPadding(new Insets(0, 0, 10, 0));
        noticeTitle.setPadding(new Insets(0, 0, 5, 0));

        // Set preferred height and width values
        noticeBox.setPrefHeight(210.0);
        noticeBox.setPrefWidth(635.0);
        noticeBody.setPrefHeight(90.0);
        noticeBody.setPrefWidth(405.0);

        // Finally add all the labels to the notice wrapper
        noticeBox.getChildren().addAll(
                noticeDate,
                noticeCollegium,
                noticeSubmitter,
                noticeTitle,
                noticeBody
        );

        return noticeBox;
    }

    public static VBox createNoticeAdminVBox(Notice notice, OnNoticeDeletedListener deletedListener) {
        VBox noticeBox = new VBox();

        // Set all the values for the notice
        Label noticeDate = new Label();
        noticeDate.setText(notice.getDateCreated().toString());
        Label noticeCollegium = new Label();
        noticeCollegium.setText("Kolegij: " + notice.getCollegium().getName());
        Label noticeSubmitter = new Label();
        noticeSubmitter.setText("Objavio/la: " + notice.getUser().getName());
        Label noticeTitle = new Label();
        noticeTitle.setText(notice.getTitle());
        Label noticeBody = new Label();
        noticeBody.setText(notice.getBody());

        HBox buttonsBox = new HBox();

        Button deleteButton = new Button();

        deleteButton.setText("Izbriši");

        deleteButton.setOnAction(event -> {
            RefRepository.deleteNotice(notice);
            deletedListener.onNoticeDeleted(notice);
        });

        // Set bottom padding for all labels
        noticeDate.setPadding(new Insets(0, 0, 5, 0));
        noticeSubmitter.setPadding(new Insets(0, 0, 10, 0));
        noticeTitle.setPadding(new Insets(0, 0, 5, 0));
        noticeBody.setPadding(new Insets(0, 0, 5, 0));

        // Set preferred height and width values
        noticeBox.setPrefHeight(210.0);
        noticeBox.setPrefWidth(635.0);
        noticeBody.setPrefHeight(90.0);
        noticeBody.setPrefWidth(405.0);

        // Add the buttons to their HBox container
        buttonsBox.getChildren().addAll(
                deleteButton
        );

        // Finally add all the labels to the notice wrapper
        noticeBox.getChildren().addAll(
                noticeDate,
                noticeCollegium,
                noticeSubmitter,
                noticeTitle,
                noticeBody,
                buttonsBox
        );

        return noticeBox;
    }

    public static void initMyNoticesButton(Button myNoticesButton, List<Notice> noticeList) {
        myNoticesButton.setText("Obavijesti (" + noticeList.size() + ")");
    }

    public static void initCollegiumComboBox(ComboBox<String> collegiumComboBox, List<Collegium> collegiumsList) {
        List<String> collegiumNameList = collegiumsList.stream().map(Collegium::getName).collect(Collectors.toList());

        collegiumComboBox.getItems().addAll(collegiumNameList);
    }

    public static Collegium getCollegiumFromComboBox(ComboBox<String> collegiumComboBox, List<Collegium> collegiumsList) {
        String selectedCollegium = collegiumComboBox.getValue();

        for (Collegium c : collegiumsList) {
            if (c.getName().equals(selectedCollegium)) {
                return c;
            }
        }

        // This shouldn't happen
        // Ever
        return null;
    }
}
