package hr.tvz.naprednajava.refmanagementt3.bil;

import hr.tvz.naprednajava.refmanagementt3.bil.exceptions.DAOException;
import hr.tvz.naprednajava.refmanagementt3.bil.interfaces.IEntityDAO;
import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Danijel on 4.7.2015..
 */
public class CollegiumDaoImpl implements IEntityDAO<Collegium> {
    private EntityManager manager;

    CollegiumDaoImpl(EntityManager mng){
        manager = mng;
    }

    @Override
    public List<Collegium> getAllFromDB() {
        manager.getTransaction().begin();
        List<Collegium> noticeList = manager.createQuery("from Collegium", Collegium.class).getResultList();
        manager.getTransaction().commit();

        return noticeList;
    }

    @Override
    public void insertIntoDB(Collegium collegium) {
        manager.getTransaction().begin();
        manager.persist(collegium);
        manager.getTransaction().commit();
    }

    @Override
    public void updateInDB(Collegium collegium) {
        manager.getTransaction().begin();
        manager.persist(collegium);
        manager.getTransaction().commit();
    }

    @Override
    public void deleteInDB(Collegium collegium) {
        manager.getTransaction().begin();
        manager.remove(collegium);
        manager.getTransaction().commit();
    }

    @Override
    public void registerEntityManager(EntityManager manager) throws DAOException {
        checkGivenManager(manager);
        this.manager = manager;
    }

    @Override
    public EntityManager getEntityManager() {
        return manager;
    }

    private void checkGivenManager(EntityManager manager) throws DAOException {
        try {
            if (!manager.isOpen())
                throw new DAOException("Received a closed EntityManager");
        } catch (NullPointerException e) {
            throw new DAOException("Null manager received", e);
        }
    }
}
