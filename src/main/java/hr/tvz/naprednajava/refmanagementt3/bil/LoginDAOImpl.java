package hr.tvz.naprednajava.refmanagementt3.bil;

import hr.tvz.naprednajava.refmanagementt3.entities.User;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Danijel on 19.6.2015..
 */
public class LoginDAOImpl {
    private EntityManager entityManager;

    public List<User> getUsers(String username) {
        entityManager.getTransaction().begin();

        List<User> userList = (List<User>) entityManager.createQuery("from User where username= :userName")
                .setParameter("userName", username).getResultList();
        entityManager.getTransaction().commit();

        return userList;
    }

    public void registerEntityManager(EntityManager manager) {
        this.entityManager = manager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void CloseEntityManager() {
        getEntityManager().close();
    }

}
