package hr.tvz.naprednajava.refmanagementt3.bil.interfaces;

import hr.tvz.naprednajava.refmanagementt3.bil.exceptions.DAOException;

import javax.persistence.EntityManager;
import java.util.List;

public interface IEntityDAO<TEntity> {

    List<TEntity> getAllFromDB();

    void insertIntoDB(TEntity entity);

    void updateInDB(TEntity entity);

    void deleteInDB(TEntity entity);

    void registerEntityManager(EntityManager manager) throws DAOException;

    EntityManager getEntityManager();

    default void closeEntitymanager() {
        getEntityManager().close();
    }
}
