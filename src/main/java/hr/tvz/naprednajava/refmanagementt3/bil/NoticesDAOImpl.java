package hr.tvz.naprednajava.refmanagementt3.bil;

import hr.tvz.naprednajava.refmanagementt3.bil.exceptions.DAOException;
import hr.tvz.naprednajava.refmanagementt3.bil.interfaces.IEntityDAO;
import hr.tvz.naprednajava.refmanagementt3.entities.Notice;

import javax.persistence.EntityManager;
import java.util.List;

public class NoticesDAOImpl implements IEntityDAO<Notice> {
    private EntityManager manager;

    NoticesDAOImpl(EntityManager mng){
        manager = mng;
    }

    public NoticesDAOImpl(){}

    @Override
    public List<Notice> getAllFromDB() {
        manager.getTransaction().begin();
        List<Notice> noticeList = manager.createQuery("from Notice", Notice.class).getResultList();
        manager.getTransaction().commit();

        return noticeList;
    }

    @Override
    public void insertIntoDB(Notice notice) {
        manager.getTransaction().begin();
        manager.persist(notice);
        manager.getTransaction().commit();
    }

    @Override
    public void updateInDB(Notice notice) {
        manager.getTransaction().begin();
        manager.persist(notice);
        manager.getTransaction().commit();
    }

    @Override
    public void deleteInDB(Notice notice) {
        manager.getTransaction().begin();
        manager.remove(notice);
        manager.getTransaction().commit();
    }

    @Override
    public void registerEntityManager(EntityManager manager) throws DAOException {
        checkGivenManager(manager);
        this.manager = manager;
    }

    @Override
    public EntityManager getEntityManager() {
        return manager;
    }

    private void checkGivenManager(EntityManager manager) throws DAOException {
        try {
            if (!manager.isOpen())
                throw new DAOException("Received a closed EntityManager");
        } catch (NullPointerException e) {
            throw new DAOException("Null manager received", e);
        }
    }
}
