package hr.tvz.naprednajava.refmanagementt3.bil.exceptions;

/**
 * Created by tomislav on 19.06.15..
 */
public class DAOException extends Exception {
    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
