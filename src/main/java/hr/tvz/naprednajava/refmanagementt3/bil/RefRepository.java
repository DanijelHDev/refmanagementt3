package hr.tvz.naprednajava.refmanagementt3.bil;

import hr.tvz.naprednajava.refmanagementt3.entities.*;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;
import hr.tvz.naprednajava.refmanagementt3.main.Main;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Danijel on 4.7.2015..
 */
public abstract class RefRepository {
    private static EntityManager manager;

    public static void initializeManager(){
        manager = Main.getEntityManager();
    }

    // Notices
    public static List<Notice> getAllNotices() {
        List<Notice> noticeList = new ArrayList<>();
        List<Collegium> collegiumList = getAllCollegiums();

        collegiumList.stream().forEach(collegium -> collegium.getNotices().stream().forEach(noticeList::add));

        return noticeList;
    }

    public static void deleteNotice(Notice notice) {
        manager.getTransaction().begin();
        manager.remove(manager.merge(notice));
        manager.getTransaction().commit();
    }

    public static List<Teacher> getAllTeachers() {
        manager.getTransaction().begin();
        List<Teacher> teacherList = manager.createQuery("from Teacher").getResultList();
        manager.getTransaction().commit();
        return teacherList;
    }

    public static List<AdministrationEmployee> getAllEmployees() {
        manager.getTransaction().begin();
        manager.flush();
        List<AdministrationEmployee> employeeList = manager.createQuery("from AdministrationEmployee").getResultList();
        manager.getTransaction().commit();
        return employeeList;
    }

    public static void insertNotice(Notice notice) {
        manager.getTransaction().begin();
        manager.persist(manager.merge(notice));
        manager.getTransaction().commit();
    }

    // *************************** Notices End

    public static List<Collegium> getAllCollegiums() {
        User currentUser = CurrentUser.GetCurrentUser();

        if(currentUser instanceof Student) {
            return ((Student) currentUser).getCollegiums();
        }else if(currentUser instanceof Teacher){
            return ((Teacher) currentUser).getCollegiums();
        }else{
            manager.getTransaction().begin();
            List<Collegium> collegiumList = manager.createQuery("from Collegium").getResultList();
            manager.getTransaction().commit();
            return collegiumList;
        }
    }

    public static List<Collegium> getAllCollegiumsForCurrentUser() {
        User user = CurrentUser.GetCurrentUser();

        return ((Teacher) user).getCollegiums();
    }

    public static void deleteCollegium(Collegium collegium) {
        manager.getTransaction().begin();
        manager.remove(collegium);
        manager.getTransaction().commit();
    }

    public static void updateUser(User user) {
        manager.getTransaction().begin();
        manager.persist(user);
        manager.getTransaction().commit();
    }

    public static void addUser(User user){
        manager.getTransaction().begin();
        manager.persist(manager.merge(user));
        manager.getTransaction().commit();
    }

    public static List<StudentCollegiumGrade> getStudentPoints(){
        Student user = (Student) CurrentUser.GetCurrentUser();
        List<StudentCollegiumGrade> studentCollegiumGradeList = new ArrayList<>();
        List<Collegium> collegiumList = user.getCollegiums();
        List<Point> studentPoints = user.getStudentPoints();
        for(Collegium collegium : collegiumList){
            List<Point> studentPointsForCollegium = studentPoints.stream()
                    .filter(point -> point.getCollegium().getId().equals(collegium.getId()))
                    .collect(Collectors.toList());
            StudentCollegiumGrade collegiumGrade = new StudentCollegiumGrade(collegium, studentPointsForCollegium);
            studentCollegiumGradeList.add(collegiumGrade);
        }
        return studentCollegiumGradeList;
    }

    public static List<Student> getAllStudents(){
        manager.getTransaction().begin();
        List<Student> listaStudenata = manager.createQuery("from Student").getResultList();
        manager.getTransaction().commit();

        return listaStudenata;
    }

    public static void updatePoints(Point p) {
        manager.getTransaction().begin();
        manager.persist(p);
        manager.getTransaction().commit();
    }

    public static void updateThresholds(Tresholds threshold) {
        manager.getTransaction().begin();
        manager.persist(threshold);
        manager.getTransaction().commit();
    }

    public static void deleteUser(User user) {
        manager.getTransaction().begin();
        manager.remove(user);
        manager.getTransaction().commit();
    }



    public static void createOrUpdateDjelatnik(AdministrationEmployee administrationEmployee) {
        manager.getTransaction().begin();
        manager.persist(manager.merge(administrationEmployee));
        manager.getTransaction().commit();
    }
}
