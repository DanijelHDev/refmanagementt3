package hr.tvz.naprednajava.refmanagementt3.listeners;

import hr.tvz.naprednajava.refmanagementt3.entities.Notice;

/**
 * Created by etombla on 4.7.2015..
 */
public interface OnNoticeDeletedListener {
    void onNoticeDeleted(Notice notice);
}
