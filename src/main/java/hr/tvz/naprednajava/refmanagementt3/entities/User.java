package hr.tvz.naprednajava.refmanagementt3.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Danijel on 17.5.2015..
 */

@Entity
@Table( name="USERS")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class User {

    @Id
    @GeneratedValue
    @Column(name="ID")
    private Long id;

    @Column(name="NAME")
    private String name;

    @Column(name="SURNAME")
    private String surname;

    @Column(name="USERNAME")
    private String username;

    @Column(name="PASSWORD")
    private String password;

    @Column(name="JMBG")
    private String jmbg;

    @Column(name="BIRTHDAY")
    private Date birthday;

    @Column(name="ADDRESS")
    private String address;

    @Column(name="CITY")
    private String city;

    @Column(name="POSTAL_CODE")
    private String postalCode;

    @Column(name="ISACTIVE")
    private String isActive;

    @OneToMany(mappedBy="user")
    private List<Notice> notices;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLES",
                joinColumns = {@JoinColumn(name = "USER_ID")},
                inverseJoinColumns = {@JoinColumn(name = "ROLE_ID")})
    private List<Role> roles;

    private String full_Name;

    //Konstruktor za Hibernate
    public User(){
        //za Hibernate
    }

    public User(String name, String surname, String username, String password, String jmbg, Date birthday, String address, String city, String postalCode, String isActive) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.jmbg = jmbg;
        this.birthday = birthday;
        this.address = address;
        this.city = city;
        this.postalCode = postalCode;
        this.isActive = isActive;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public List<Notice> getNotices() {
        return notices;
    }

    public void setNotices(List<Notice> notices) {
        this.notices = notices;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getFullName() {
        String full_Name = this.name + " "+ this.surname;
        return full_Name;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive (String isActive) {
        this.isActive = isActive;
    }
}
