package hr.tvz.naprednajava.refmanagementt3.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Danijel on 17.5.2015..
 */
@Entity
@Table(name="TEACHERS")
@PrimaryKeyJoinColumn(name="ID")
public class Teacher extends User {

    @Column(name="JMBAG")
    private String jmbag;

    @OneToMany(mappedBy = "teacher")
    private List<Collegium> collegiums;

    public Teacher(){
        //za Hibernate
    }

    public Teacher(String name, String surname, String username, String password, String jmbg, Date birthday, String address, String city, String postalCode, String jmbag, String isActive) {
        super(name, surname, username, password, jmbg, birthday, address, city, postalCode, isActive);
        this.jmbag = jmbag;
    }

    public String getJmbag() {
        return jmbag;
    }

    public void setJmbag(String jmbag) {
        this.jmbag = jmbag;
    }

    public List<Collegium> getCollegiums() {
        return collegiums;
    }
}

