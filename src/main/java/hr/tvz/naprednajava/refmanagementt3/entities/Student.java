package hr.tvz.naprednajava.refmanagementt3.entities;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Danijel on 17.5.2015..
 */

@Entity
@Table(name="STUDENTS")

@PrimaryKeyJoinColumn(name="ID")
public class Student extends User {

    @Column(name="JMBAG")
    private String jmbag;

    @OneToMany(mappedBy = "student")
    private List<Point> studentPoints;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "STUDENT_COLLEGIUM",
            joinColumns = {@JoinColumn(name = "STUDENT_ID")},
            inverseJoinColumns = {@JoinColumn(name = "COLLEGIUM_ID")})
    private List<Collegium> collegiums;

    //Konstruktor za Hibernate
    public Student(){}

    public Student(String name, String surname, String username, String password, String jmbg, Date birthday, String address, String city, String postalCode, String jmbag, String isActive) {
        super(name, surname, username, password, jmbg, birthday, address, city, postalCode, isActive);
        this.jmbag = jmbag;

    }

    public String getJmbag() {
        return jmbag;
    }

    public void setJmbag(String jmbag) {
        this.jmbag = jmbag;
    }

    public List<Point> getStudentPoints() {
        return studentPoints;
    }

    public void setStudentPoints(List<Point> studentPoints) {
        this.studentPoints = studentPoints;
    }

    public List<Collegium> getCollegiums() {
        return collegiums;
    }

    public void setCollegiums(List<Collegium> collegiums) {
        this.collegiums = collegiums;
    }
}
