package hr.tvz.naprednajava.refmanagementt3.entities;

import javax.persistence.*;
import java.util.Date;

@PrimaryKeyJoinColumn(name="ID")
@Entity
@Table(name = "ADMINISTRATION_EMPLOYEE")

public class AdministrationEmployee extends User {

    @Column(name="OIB")
    private String oib;

    public AdministrationEmployee(){}

    public AdministrationEmployee(String name, String surname, String username, String password, String jmbg, Date birthday, String address, String city, String postalCode, String oib,String isActive) {
        super(name, surname, username, password, jmbg, birthday, address, city, postalCode,isActive);
        this.oib = oib;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

}
