package hr.tvz.naprednajava.refmanagementt3.entities;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.main.CurrentUser;

import java.util.List;

/**
 * Created by irusan on 9.7.2015..
 */
public class StudentCollegiumGrade {
    private String semester;
    private String collegiumName;
    private int labPoints;
    private int colloquiumPoints;
    private int extraPoints;
    private int totalPoints;
    private int grade;

    public StudentCollegiumGrade(){}

    public StudentCollegiumGrade(Collegium collegium, List<Point> studentPointsForCollegium){
        this.collegiumName = collegium.getName();
        this.semester = collegium.getSemester();
        for(Point point : studentPointsForCollegium){
            if((point.getType().name()).equals("LABORATORY")){
                this.labPoints = point.getPoints();
            }else if((point.getType().name()).equals("COLLOQUIUM")) {
                this.colloquiumPoints = point.getPoints();
            }else{
                this.extraPoints = point.getPoints();
            }
        this.totalPoints = labPoints + colloquiumPoints + extraPoints;
        }
        for(Tresholds treshold : collegium.getTresholds()){
            if(totalPoints < (treshold.getMaxPoints() + 1) && totalPoints  >= treshold.getMinPoints()){
                this.grade = treshold.getScore();
            }
        }

    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getCollegiumName() {
        return collegiumName;
    }

    public void setCollegiumName(String collegiumName) {
        this.collegiumName = collegiumName;
    }

    public int getLabPoints() {
        return labPoints;
    }

    public void setLabPoints(int labPoints) {
        this.labPoints = labPoints;
    }

    public int getColloquiumPoints() {
        return colloquiumPoints;
    }

    public void setColloquiumPoints(int colloquiumPoints) {
        this.colloquiumPoints = colloquiumPoints;
    }

    public int getExtraPoints() {
        return extraPoints;
    }

    public void setExtraPoints(int extraPoints) {
        this.extraPoints = extraPoints;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
