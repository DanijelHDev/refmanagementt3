package hr.tvz.naprednajava.refmanagementt3.entities;

/**
 * Created by irusan on 4.7.2015..
 */
public enum Type {
    COLLOQUIUM, LABORATORY, EXTRA_POINTS
}
