package hr.tvz.naprednajava.refmanagementt3.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ADMINS")
@PrimaryKeyJoinColumn(name = "ID")
public class Cafuta extends User {

    public Cafuta() {
        // Prazan konstruktor za Hibernate
    }

    public Cafuta(String name, String surname, String username, String password,
                  String jmbg, Date birthday, String address, String city, String postalCode,String isActive) {
        super(name, surname, username, password, jmbg, birthday, address, city, postalCode,isActive);
    }

}