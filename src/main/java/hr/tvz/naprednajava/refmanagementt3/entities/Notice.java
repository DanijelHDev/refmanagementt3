package hr.tvz.naprednajava.refmanagementt3.entities;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Danijel on 17.5.2015..
 */
@Entity
@Table(name = "NOTICES")
public class Notice {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name="USER_ID")
    private User user;

    @ManyToOne
    @JoinColumn(name="COLLEGIUM_ID")
    private Collegium collegium;

    @Column(name="TITLE")
    private String title;

    @Column(name="DESCRIPTION")
    private String description;

    @Type(type="timestamp")
    @Column(name="date")
    private Date dateCreated;

    @Type(type="timestamp")
    @Column(name="expiration_Date")
    private Date expirationDate;

    public Notice() {
        // Empty contruktor for Hibernate
    }

    public Notice(User user, Collegium collegium, String title, String description, Date dateCreated, Date expirationDate) {
        this.user = user;
        this.collegium = collegium;
        this.title = title;
        this.description = description;
        this.dateCreated = dateCreated;
        this.expirationDate = expirationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Collegium getCollegium() {
        return collegium;
    }

    public void setCollegium(Collegium collegium) {
        this.collegium = collegium;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return description;
    }

    public void setBody(String body) {
        this.description = body;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isArhived(){
        return expirationDate.before(new Date());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

}
