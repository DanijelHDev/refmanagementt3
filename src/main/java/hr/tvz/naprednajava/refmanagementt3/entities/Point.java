package hr.tvz.naprednajava.refmanagementt3.entities;

import javax.persistence.*;

/**
 * Created by irusan on 4.7.2015..
 */
@Entity
@Table(name = "POINTS")
public class Point {

    @Id
    @GeneratedValue
    @Column(name="ID")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE")
    private Type type;

    @Column(name = "POINTS")
    private int points;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "collegium_id")
    private Collegium collegium;

    @ManyToOne
    @JoinColumn(name = "studentPoints")
    private Student student;

    public Point(){
        //za Hibernate
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collegium getCollegium() {
        return collegium;
    }

    public void setCollegium(Collegium collegium) {
        this.collegium = collegium;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
