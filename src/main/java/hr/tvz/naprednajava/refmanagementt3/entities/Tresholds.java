package hr.tvz.naprednajava.refmanagementt3.entities;

import javax.persistence.*;

/**
 * Created by irusan on 4.7.2015..
 */

@Entity
@Table(name = "TRESHOLDS")
public class Tresholds {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "MIN_POINTS")
    private int minPoints;

    @Column(name = "MAX_POINTS")
    private int maxPoints;

    @Column(name = "SCORE")
    private int score;

    @ManyToOne
    @JoinColumn(name = "COLLEGIUM_ID")
    private Collegium collegium;

    public Tresholds(){
        // za Hibernate
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(int minPoints) {
        this.minPoints = minPoints;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Collegium getCollegium() {
        return collegium;
    }

    public void setCollegium(Collegium collegium) {
        this.collegium = collegium;
    }
}
