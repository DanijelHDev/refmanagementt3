package hr.tvz.naprednajava.refmanagementt3.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danijel on 17.5.2015..
 */
@Entity
@Table(name = "COLLEGIUMS")
public class Collegium {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ECTS")
    private String ects;

    @Column(name = "SEMESTER")
    private String semester;

    @ManyToOne()
    @JoinColumn(name = "TEACHER_ID")
    private Teacher teacher;

    @OneToMany(mappedBy="collegium")
    private List<Notice> notices;

    @OneToMany(mappedBy = "collegium")
    private List<Point> points;

    @ManyToMany(mappedBy = "collegiums")
    private List<Student> students;

    @OneToMany(mappedBy = "collegium")
    private List<Tresholds> tresholds;

    public Collegium() {
        //Za hibernate
    }

    public Collegium(String name, String description, String ects) {
        this.name = name;
        this.description = description;
        this.ects = ects;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getEcts() {
        return ects;
    }

    public void setEcts(String ects) {
        this.ects = ects;
    }

    public void setNotices(List<Notice> notices) {
        this.notices = notices;
    }

    public User getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public List<Notice> getNotices() {
        return notices;
    }



    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }


    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<Tresholds> getTresholds() {
        return tresholds;
    }

    public void setTresholds(List<Tresholds> tresholds) {
        this.tresholds = tresholds;
    }
}
