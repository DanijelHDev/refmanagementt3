package hr.tvz.naprednajava.refmanagementt3.bll;

import hr.tvz.naprednajava.refmanagementt3.JUnitHibernateConfiguration;
import hr.tvz.naprednajava.refmanagementt3.entities.*;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Danijel on 10.7.2015..
 */
public class RefRepoTestJUnit {
    private Session session = null;
    JUnitHibernateConfiguration conf = new JUnitHibernateConfiguration();

    @Before
    public void SetUpHibernateSession() {
        session = conf.getSessionFactory();
    }

    @Test
    public void getAllNotices(){
        session.getTransaction().begin();
        List<Notice> noticeList = session.createQuery("from Collegium").list();
        session.getTransaction().commit();
        assertTrue(noticeList.size()!=0);
    }

    @Test
    public void getAllCollegiums(){
        session.getTransaction().begin();
        List<Collegium> collegiumList = session.createQuery("from Collegium").list();
        session.getTransaction().commit();
        assertTrue(collegiumList.size()!=0);
    }

    @Test
    public void getAllEmployees(){
        session.getTransaction().begin();
        List<AdministrationEmployee> employeeList = session.createQuery("from AdministrationEmployee").list();
        session.getTransaction().commit();
        assertTrue(employeeList.size()!=0);
    }

    @Test
    public void getAllStudents(){
        session.getTransaction().begin();
        List<Student> listaStudenata = session.createQuery("from Student").list();
        session.getTransaction().commit();
        assertTrue(listaStudenata.size()!=0);

    }

    @Test
    public void getAllTeachers(){
        session.getTransaction().begin();
        List<Teacher> teacherList = session.createQuery("from Teacher").list();
        session.getTransaction().commit();
        assertTrue(teacherList.size()!=0);
    }

    @Test
    public void deleteNotice(){
        long maxID = 0;
        session.getTransaction().begin();
        List<Notice> noticeList = session.createQuery("from Notice").list();
        session.getTransaction().commit();
        Notice zaDelete = new Notice();
        for(Notice not : noticeList){
            if(not.getId() > maxID){
                maxID = not.getId();
                zaDelete = not;
            }
        }
        session.getTransaction().begin();
        session.delete(zaDelete);
        session.getTransaction().commit();
        session.getTransaction().begin();
        List<Notice> newList = session.createQuery("from Notice").list();
        session.getTransaction().commit();

        assertTrue(newList.size() < noticeList.size());
    }

    @Test
    public void insertNotice(){
        Notice newNotice;

        session.getTransaction().begin();
        List<Collegium> collegiumList = (List<Collegium>)session.createQuery("from Collegium").list();
        session.getTransaction().commit();
        Collegium collegium = collegiumList.get(0);

        session.getTransaction().begin();
        List<User> userList = (List<User>)session.createQuery("from User").list();
        session.getTransaction().commit();
        User user = userList.get(0);

        session.getTransaction().begin();
        List<Notice> noticeList = session.createQuery("from Notice").list();
        session.getTransaction().commit();
        noticeList.size();

        newNotice = new Notice(user, collegium, "Title", "Description", new Date(), new Date());
        session.getTransaction().begin();
        session.persist(newNotice);
        session.getTransaction().commit();

        session.getTransaction().begin();
        List<Notice> newNoticeList = session.createQuery("from Notice").list();
        session.getTransaction().commit();

        assertTrue(newNoticeList.size() > noticeList.size());
    }

    @After
    public void after() {
        conf.closeSession();
    }
}
