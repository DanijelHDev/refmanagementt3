package hr.tvz.naprednajava.refmanagementt3;

import hr.tvz.naprednajava.refmanagementt3.entities.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

/**
 * Created by Danijel on 19.6.2015..
 */
public class JUnitHibernateConfiguration {

    private ServiceRegistry serviceRegistry;
    private SessionFactory sessionFactory;
    private Session session = null;

    public Session getSessionFactory() {
        Configuration configuration = new Configuration();

        String connectionUrl = "jdbc:sqlserver://junittestserver.database.windows.net;" +
                "databaseName=JUnitTestDB;user=JUnitGuru@junittestserver;password=guruJUnit123";

        configuration.addAnnotatedClass(User.class)
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Teacher.class)
                .addAnnotatedClass(Notice.class)
                .addAnnotatedClass(Collegium.class)
                .addAnnotatedClass(AdministrationEmployee.class)
                .addAnnotatedClass(Cafuta.class);

        //Testna baza na Microsoft Azure
        configuration.setProperty(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
        configuration.setProperty(Environment.URL, connectionUrl);
        configuration.setProperty(Environment.USER, "JUnitGuru");
        configuration.setProperty(Environment.PASS, "guruJUnit123");
        configuration.setProperty(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");
        configuration.addAnnotatedClass(AdministrationEmployee.class);
        configuration.addAnnotatedClass(Cafuta.class);
        configuration.addAnnotatedClass(Collegium.class);
        configuration.addAnnotatedClass(Notice.class);
        configuration.addAnnotatedClass(Point.class);
        configuration.addAnnotatedClass(Role.class);
        configuration.addAnnotatedClass(Student.class);
        configuration.addAnnotatedClass(Teacher.class);
        configuration.addAnnotatedClass(Tresholds.class);
        configuration.addAnnotatedClass(Type.class);
        configuration.addAnnotatedClass(User.class);

        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();

        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        session = sessionFactory.openSession();

        return session;
    }

    public void closeSession() {
        session.close();
        sessionFactory.close();
    }

}
