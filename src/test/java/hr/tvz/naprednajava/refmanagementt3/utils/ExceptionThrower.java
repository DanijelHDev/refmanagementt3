package hr.tvz.naprednajava.refmanagementt3.utils;

@FunctionalInterface
public interface ExceptionThrower {
    void throwException() throws Throwable;
}