package hr.tvz.naprednajava.refmanagementt3.utils;

import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.entities.Notice;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by etombla on 4.7.2015..
 */
public abstract class ExpectedReturns {
    public static List<Notice> NOTICES = new ArrayList<Notice>() {{
        add(new Notice());
        add(new Notice());
        add(new Notice());
    }};

    public static List<Collegium> COLLEGIUMS = new ArrayList<Collegium>() {{
        add(new Collegium());
        add(new Collegium());
        add(new Collegium());
    }};
}
