package hr.tvz.naprednajava.refmanagementt3.testfx;

import hr.tvz.naprednajava.refmanagementt3.entities.Collegium;
import hr.tvz.naprednajava.refmanagementt3.entities.Notice;
import hr.tvz.naprednajava.refmanagementt3.entities.Teacher;
import hr.tvz.naprednajava.refmanagementt3.entities.User;
import hr.tvz.naprednajava.refmanagementt3.utils.NoticesUtils;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NoticesUtilsFXTest extends ApplicationTest {
    private Notice testNotice;

    @Override
    public void start(Stage stage) throws Exception {
        // Empty method required to initialize the JavaFX toolkit
    }
/*
    @Before
    public void setup() {
        User noticeSubmitter = new Teacher(
                "Aleksander",
                "Radovan",
                "aradovan",
                "radopass",
                "123456789",
                new Date(),
                "Ulica Neka 14",
                "Zagreb",
                "10000",
                "024601234567",
                "true");

        Collegium collegium = new Collegium("Napredna JAVA",
                "Java, Lambde, Jenkins, Sonar, Azure, Postres, IntelliJ IDEA",
                "20");

        testNotice = new Notice(noticeSubmitter, collegium, "Title", "Description", new Date(), new Date());
    }

    @Test
    public void verifyCreateNoticeVBoxContentStudent() {
        VBox retrievedContainer = NoticesUtils.createNoticeVBox(testNotice);

        assertEquals(5, retrievedContainer.getChildren().size());
        assertTrue(retrievedContainer.getChildren().get(0) instanceof Label);
        assertTrue(retrievedContainer.getChildren().get(1) instanceof Label);
        assertTrue(retrievedContainer.getChildren().get(2) instanceof Label);
        assertTrue(retrievedContainer.getChildren().get(3) instanceof Label);
        assertTrue(retrievedContainer.getChildren().get(4) instanceof Label);
    }

    @Test
    public void verifyCreateNoticeVBoxContentValuesStudent() {
        VBox retrievedContainer = NoticesUtils.createNoticeVBox(testNotice);

        assertEquals(5, retrievedContainer.getChildren().size());
        assertTrue(((Label) retrievedContainer.getChildren().get(0)).getText().equals(testNotice.getDateCreated().toString()));
        assertTrue(((Label) retrievedContainer.getChildren().get(1)).getText().equals("Kolegij: " + testNotice.getCollegium().getName()));
        assertTrue(((Label) retrievedContainer.getChildren().get(2)).getText().equals("Objavio/la: " + testNotice.getUser().getName()));
        assertTrue(((Label) retrievedContainer.getChildren().get(3)).getText().equals(testNotice.getTitle()));
        assertTrue(((Label) retrievedContainer.getChildren().get(4)).getText().equals(testNotice.getBody()));
    }

    @Test
    public void verifyCreateNoticeVBoxContentAdmin() {
        VBox retrievedContainer = NoticesUtils.createNoticeAdminVBox(testNotice, null);

        assertEquals(6, retrievedContainer.getChildren().size());
        assertTrue(retrievedContainer.getChildren().get(0) instanceof Label);
        assertTrue(retrievedContainer.getChildren().get(1) instanceof Label);
        assertTrue(retrievedContainer.getChildren().get(2) instanceof Label);
        assertTrue(retrievedContainer.getChildren().get(3) instanceof Label);
        assertTrue(retrievedContainer.getChildren().get(4) instanceof Label);
        assertTrue(retrievedContainer.getChildren().get(5) instanceof HBox);

        HBox buttonContainer = (HBox) retrievedContainer.getChildren().get(5);
        assertTrue(buttonContainer.getChildren().get(0) instanceof Button);
    }

    @Test
    public void verifyCreateNoticeVBoxContentValuesAdmin() {
        VBox retrievedContainer = NoticesUtils.createNoticeAdminVBox(testNotice, null);

        assertEquals(6, retrievedContainer.getChildren().size());
        assertTrue(((Label) retrievedContainer.getChildren().get(0)).getText().equals(testNotice.getDateCreated().toString()));
        assertTrue(((Label) retrievedContainer.getChildren().get(1)).getText().equals("Kolegij: " + testNotice.getCollegium().getName()));
        assertTrue(((Label) retrievedContainer.getChildren().get(2)).getText().equals("Objavio/la: " + testNotice.getUser().getName()));
        assertTrue(((Label) retrievedContainer.getChildren().get(3)).getText().equals(testNotice.getTitle()));
        assertTrue(((Label) retrievedContainer.getChildren().get(4)).getText().equals(testNotice.getBody()));

        HBox buttonContainer = (HBox) retrievedContainer.getChildren().get(5);
        assertTrue(((Button) buttonContainer.getChildren().get(0)).getText().equals("Izbriši"));
    }
    */
}
