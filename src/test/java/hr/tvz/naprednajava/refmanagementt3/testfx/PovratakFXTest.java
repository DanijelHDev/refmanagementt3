package hr.tvz.naprednajava.refmanagementt3.testfx;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.matcher.base.NodeMatchers;

import static org.testfx.api.FxAssert.verifyThat;

/**
 * Created by Nikola on 19.6.2015..
 */
public class PovratakFXTest extends ApplicationTest {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/screens/administrator/createAdministrator.fxml"));
        Scene scene = new Scene(root, 500, 500);
        scene.getStylesheets().add("stylesheets/style.css");
        stage.setScene(scene);
        stage.show();
    }
/*
    @Test
    public void testBackButton() {
        clickOn("#buttPovratak");
        verifyThat("#firstNameTextField", NodeMatchers.isNotNull());
    }
    */
}

