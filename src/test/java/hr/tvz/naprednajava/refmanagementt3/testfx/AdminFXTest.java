package hr.tvz.naprednajava.refmanagementt3.testfx;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.matcher.base.NodeMatchers;

import static org.testfx.api.FxAssert.verifyThat;

/**
 * Created by Nikola on 19.6.2015..
 */
public class AdminFXTest extends ApplicationTest {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/screens/administrator/Cafuta.fxml"));
        Scene scene = new Scene(root, 500, 500);
        scene.getStylesheets().add("stylesheets/style.css");
        stage.setScene(scene);
        stage.show();
    }
/*
    @Test
    public void testCreateAdminScreen() {
        clickOn("#buttCreateAdmin");
        verifyThat("#firstNameTextField", NodeMatchers.isVisible());
        verifyThat("#lastNameTextField", NodeMatchers.isVisible());
        verifyThat("#usernameTextField", NodeMatchers.isVisible());
        verifyThat("#passwordTextField", NodeMatchers.isVisible());
        verifyThat("#retypePasswordTextField", NodeMatchers.isVisible());
        verifyThat("#jmbgTextField", NodeMatchers.isVisible());
        verifyThat("#birthdayPicker", NodeMatchers.isVisible());
        verifyThat("#addressTextField", NodeMatchers.isVisible());
        verifyThat("#cityTextField", NodeMatchers.isVisible());
        verifyThat("#postalCodeTextField", NodeMatchers.isVisible());

        clickOn("#firstNameTextField").write("Davor");
        clickOn("#lastNameTextField").write("Cafuta");
        clickOn("#usernameTextField").write("dcafuta");
        clickOn("#passwordTextField").write("cafi");
        clickOn("#retypePasswordTextField").write("cafi");
        clickOn("#jmbgTextField").write("123456789");
        clickOn("#birthdayPicker").write("2015-01-01");
        clickOn("#addressTextField").write("Adresa 123");
        clickOn("#cityTextField").write("Zagreb");
        clickOn("#postalCodeTextField").write("10410");
        clickOn("#createAdmin");

        //verifyThat("");
    }
    */
}
