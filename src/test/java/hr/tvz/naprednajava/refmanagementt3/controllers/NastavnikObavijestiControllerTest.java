package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.controllers.nastavnik.NastavnikObavijestiController;
import hr.tvz.naprednajava.refmanagementt3.utils.ExpectedReturns;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static org.easymock.EasyMock.expect;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replay;
import static org.powermock.api.easymock.PowerMock.verify;

/**
 * Created by etombla on 4.7.2015..
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(RefRepository.class)
public class NastavnikObavijestiControllerTest {
    @Test
    public void verifyNoticeListRetrieval() {
        mockStatic(RefRepository.class);
        expect(RefRepository.getAllNotices()).andReturn(ExpectedReturns.NOTICES);

        hr.tvz.naprednajava.refmanagementt3.controllers.nastavnik.NastavnikObavijestiController controller = new hr.tvz.naprednajava.refmanagementt3.controllers.nastavnik.NastavnikObavijestiController();

        replay(RefRepository.class);
        controller.retrieveNoticeList();
        verify(RefRepository.class);

        assertEquals(ExpectedReturns.NOTICES, controller.getNoticeList());
    }

    @Test
    public void verifyCollegiumListRetrieval() {
        mockStatic(RefRepository.class);
        expect(RefRepository.getAllCollegiumsForCurrentUser()).andReturn(ExpectedReturns.COLLEGIUMS);

        NastavnikObavijestiController controller = new NastavnikObavijestiController();

        replay(RefRepository.class);
        controller.retrieveCollegiumsList();
        verify(RefRepository.class);

        assertEquals(ExpectedReturns.COLLEGIUMS, controller.getCollegiumsList());
    }
}
