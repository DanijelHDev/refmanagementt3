package hr.tvz.naprednajava.refmanagementt3.controllers;

import hr.tvz.naprednajava.refmanagementt3.bil.RefRepository;
import hr.tvz.naprednajava.refmanagementt3.controllers.student.StudentObavijestiController;
import hr.tvz.naprednajava.refmanagementt3.utils.ExpectedReturns;
import org.easymock.IAnswer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.concurrent.atomic.AtomicBoolean;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.easymock.EasyMock.*;
import static org.powermock.api.easymock.PowerMock.mockStatic;

/**
 * Created by etombla on 4.7.2015..
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(RefRepository.class)
public class StudentObavijestiControllerTest {

//    @Test
//    public void test() {
//        final AtomicBoolean methodCalled = new AtomicBoolean();
//        mockStatic(RefRepository.class);
//        expect(RefRepository.getAllNotices()).andReturn(ExpectedReturns.NOTICES);
//
//        StudentObavijestiController controller = new StudentObavijestiController();
//        controller.retrieveNoticeList();
//        expectLastCall().andAnswer(() -> {
//            methodCalled.set(true);
//            return null;
//        });
//
//        replay(RefRepository.class);
//        replay(controller);
//        controller.getNoticeList();
//        verify(controller);
//        verify(RefRepository.class);
//
//        assertTrue("Method isn't called", methodCalled.get());
//    }

    @Test
    public void verifyNoticeListRetrieval() {
        mockStatic(RefRepository.class);
        expect(RefRepository.getAllNotices()).andReturn(ExpectedReturns.NOTICES);

        StudentObavijestiController controller = new StudentObavijestiController();

        PowerMock.replay(RefRepository.class);
        controller.retrieveNoticeList();
        PowerMock.verify(RefRepository.class);

        assertEquals(ExpectedReturns.NOTICES, controller.getNoticeList());
    }
}
