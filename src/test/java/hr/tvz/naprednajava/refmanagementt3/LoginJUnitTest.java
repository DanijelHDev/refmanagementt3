package hr.tvz.naprednajava.refmanagementt3;

import hr.tvz.naprednajava.refmanagementt3.entities.Student;
import hr.tvz.naprednajava.refmanagementt3.entities.Teacher;
import hr.tvz.naprednajava.refmanagementt3.entities.User;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Created by Danijel on 17.6.2015..
 */
public class LoginJUnitTest {
    private Session session = null;
    JUnitHibernateConfiguration conf = new JUnitHibernateConfiguration();

    @Before
    public void SetUpHibernateSession() {
        session = conf.getSessionFactory();
    }

    @Test
    public void ObjectInicializationTest() {
        User user = new Student();
        user.setName("Danijel");
        user.setJmbg("123456");
        user.setSurname("Hrcek");
        user.setPassword("password");
        user.setAddress("Đurmanec 165");
        user.setCity("Đurmanec");
        user.setPostalCode("49225");
        user.setUsername("dhrcek");

        assertEquals("dhrcek", user.getUsername());
        assertTrue(user != null);
    }

    @Test
    public void studentLoginTest() {
        User student = new Student();
        student.setName("Danijel");
        student.setSurname("Hrcek");
        student.setUsername("jdaniels");

        assertEquals("jdaniels", student.getUsername());

        session.getTransaction().begin();
        List<User> listaUser = (List<User>) session.createQuery("from User where username=:userName")
                .setParameter("userName", student.getUsername()).list();
        session.getTransaction().commit();

        student = listaUser.get(0);

        assertTrue(student.getUsername().equals("jdaniels"));
        assertTrue(student.getPassword().equals("wnbpass"));

        assertTrue(listaUser.size() != 0);
    }

    @Test
    public void teacherLoginTest(){
        User teacher = new Teacher();
        teacher.setName("Aleksander");
        teacher.setSurname("Radovan");
        teacher.setUsername("aradovan");

        session.getTransaction().begin();
        List<User> listaUser = (List<User>) session.createQuery("from User where username=:userName")
                .setParameter("userName", teacher.getUsername()).list();
        session.getTransaction().commit();

        teacher = listaUser.get(0);

        assertTrue(teacher.getUsername().equals("aradovan"));
        assertTrue(teacher.getPassword().equals("pass"));

        assertTrue(listaUser.size() != 0);
    }

    @After
    public void after() {
        conf.closeSession();
    }

}
