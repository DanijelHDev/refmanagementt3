CREATE SCHEMA "refmanagmentt3database";

CREATE TABLE "refmanagmentt3database"."courses" ( 
	"id"                 integer   NOT NULL,
	"name"               varchar(40)   NOT NULL,
	CONSTRAINT "pk_smjerovi" PRIMARY KEY ( "id" )
 );

CREATE TABLE "refmanagmentt3database"."roles" ( 
	"id"                 integer   NOT NULL,
	"name"               varchar(100)  NOT NULL,
	"description"        longvarchar   ,
	CONSTRAINT "pk_roles" PRIMARY KEY ( "id" )
 );

CREATE TABLE "refmanagmentt3database"."users" ( 
	"id"                 integer   NOT NULL,
	"jmbg"               varchar(13)   NOT NULL,
	"name"               varchar(20)   NOT NULL,
	"surname"            varchar(20)   NOT NULL,
	"username"           varchar(15)   NOT NULL,
	"password"           char(32)   ,
	"birthday"           date   NOT NULL,
	"address"            varchar(20)   NOT NULL,
	"city"               varchar(15)   NOT NULL,
	"postal_code"        integer   NOT NULL,
	CONSTRAINT "pk_users" PRIMARY KEY ( "id" )
 );

CREATE TABLE "refmanagmentt3database"."students" ( 
	"id"                 integer   NOT NULL,
	"jmbag"              integer   NOT NULL,
	COURSE_ID            integer   NOT NULL,
	CONSTRAINT "pk_students" PRIMARY KEY ( "jmbag" ),
	CONSTRAINT "pk_studenti" UNIQUE ( COURSE_ID ) ,
	CONSTRAINT "idx_studenti" UNIQUE ( "id" ) 
 );

CREATE TABLE "refmanagmentt3database"."teachers" ( 
	"id"                 integer   NOT NULL,
	"title"              integer   NOT NULL,
	"collegium_id"       integer   NOT NULL,
	CONSTRAINT "pk_profesori" PRIMARY KEY ( "id" ),
	CONSTRAINT "pk_profesori_1" UNIQUE ( "collegium_id" ) 
 );

CREATE TABLE "refmanagmentt3database"."user_ROLES" ( 
	"id"                 integer   NOT NULL,
	"role_id"            integer   NOT NULL,
	"user_id"            integer   NOT NULL,
	CONSTRAINT "pk_user_roles" PRIMARY KEY ( "id" ),
	CONSTRAINT "pk_user_roles_0" UNIQUE ( "role_id" ) ,
	CONSTRAINT "pk_user_roles_1" UNIQUE ( "user_id" ) 
 );

CREATE TABLE "refmanagmentt3database"."student_collegium" ( 
	"id"                 integer   NOT NULL,
	"student_id"         integer   NOT NULL,
	"collegium_id"       integer   NOT NULL,
	CONSTRAINT "idx_student_kolegij" PRIMARY KEY ( "student_id", "collegium_id" ),
	CONSTRAINT "pk_student_kolegij" UNIQUE ( "student_id" ) ,
	CONSTRAINT "pk_student_kolegij_0" UNIQUE ( "collegium_id" ) 
 );

CREATE TABLE "refmanagmentt3database"."collegiums" ( 
	"id"                 integer   NOT NULL,
	"name"               varchar(40)   NOT NULL,
	"description"        longvarchar   ,
	"ects_point"         longvarchar   NOT NULL,
	"course_id"          integer   NOT NULL,
	"teacher_id"         integer   NOT NULL,
	CONSTRAINT "pk_kolegiji" PRIMARY KEY ( "id" ),
	CONSTRAINT "pk_kolegiji_0" UNIQUE ( "course_id" ) ,
	CONSTRAINT "pk_kolegiji_1" UNIQUE ( "teacher_id" ) 
 );

CREATE TABLE "refmanagmentt3database"."notices" ( 
	"id"                 integer   NOT NULL,
	"title"              varchar(100)   NOT NULL,
	"description"        longvarchar   NOT NULL,
	"user_id"            integer   NOT NULL,
	"collegium_id"       integer   ,
	CONSTRAINT "pk_notices" PRIMARY KEY ( "id" ),
	CONSTRAINT "pk_notices_0" UNIQUE ( "collegium_id" ) ,
	CONSTRAINT "pk_notices_1" UNIQUE ( "user_id" ) 
 );

CREATE TABLE "refmanagmentt3database"."points" ( 
	"id"                 integer   NOT NULL,
	TYPE                 varchar(10)   NOT NULL,
	"points"             integer   NOT NULL,
	"description"        longvarchar   ,
	COLLEGIUM_ID         integer  NOT NULL,
	STUDENT_JMBAG        integer   NOT NULL,
	CONSTRAINT "pk_bodovi_kolokviji" PRIMARY KEY ( "id" ),
	CONSTRAINT "pk_bodovi_kolokviji_0" UNIQUE ( COLLEGIUM_ID ) 
 );

CREATE INDEX IDX_POINTS ON "refmanagmentt3database"."points" ( STUDENT_JMBAG );

CREATE INDEX IDX_POINTS_0 ON "refmanagmentt3database"."points" ( TYPE );

CREATE TABLE "refmanagmentt3database"."thresholds" ( 
	"id"                 integer   NOT NULL,
	"min_points"         integer   NOT NULL,
	"max_points"         integer   NOT NULL,
	"score"              integer   NOT NULL,
	"collegium_id"       integer   ,
	CONSTRAINT "pk_bodovni prag" PRIMARY KEY ( "id" ),
	CONSTRAINT "pk_bodovni_pragovi" UNIQUE ( "collegium_id" ) 
 );

ALTER TABLE "refmanagmentt3database"."collegiums" ADD CONSTRAINT "fk_kolegiji_0" FOREIGN KEY ( "id" ) REFERENCES "refmanagmentt3database"."student_collegium"( "collegium_id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."collegiums" ADD CONSTRAINT FK_COLLEGIUMS FOREIGN KEY ( "teacher_id" ) REFERENCES "refmanagmentt3database"."teachers"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."collegiums" ADD CONSTRAINT FK_COLLEGIUMS_0 FOREIGN KEY ( "course_id" ) REFERENCES "refmanagmentt3database"."courses"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."notices" ADD CONSTRAINT FK_NOTICES FOREIGN KEY ( "user_id" ) REFERENCES "refmanagmentt3database"."users"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."notices" ADD CONSTRAINT FK_NOTICES_0 FOREIGN KEY ( "collegium_id" ) REFERENCES "refmanagmentt3database"."collegiums"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."points" ADD CONSTRAINT FK_POINTS FOREIGN KEY ( STUDENT_JMBAG ) REFERENCES "refmanagmentt3database"."students"( "jmbag" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."points" ADD CONSTRAINT FK_POINTS_1 FOREIGN KEY ( COLLEGIUM_ID ) REFERENCES "refmanagmentt3database"."collegiums"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."student_collegium" ADD CONSTRAINT FK_STUDENT_COLLEGIUM FOREIGN KEY ( "student_id" ) REFERENCES "refmanagmentt3database"."students"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."students" ADD CONSTRAINT FK_STUDENTS_0 FOREIGN KEY ( COURSE_ID ) REFERENCES "refmanagmentt3database"."courses"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."students" ADD CONSTRAINT FK_STUDENTS FOREIGN KEY ( "id" ) REFERENCES "refmanagmentt3database"."users"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."teachers" ADD CONSTRAINT FK_TEACHERS FOREIGN KEY ( "id" ) REFERENCES "refmanagmentt3database"."users"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."thresholds" ADD CONSTRAINT FK_THRESHOLDS FOREIGN KEY ( "collegium_id" ) REFERENCES "refmanagmentt3database"."collegiums"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."user_ROLES" ADD CONSTRAINT FK_USER FOREIGN KEY ( "role_id" ) REFERENCES "refmanagmentt3database"."roles"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "refmanagmentt3database"."user_ROLES" ADD CONSTRAINT FK_USER_0 FOREIGN KEY ( "user_id" ) REFERENCES "refmanagmentt3database"."users"( "id" ) ON DELETE NO ACTION ON UPDATE NO ACTION;


